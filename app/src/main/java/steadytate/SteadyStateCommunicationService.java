package steadytate;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class SteadyStateCommunicationService extends Service
{
  static final int IS_ALIVE = 2;
  static final int IS_MDM = 3;
  static final int MSG_SAY_HELLO = 1;
  static final int MSG_SAY_HELLO_NO_HOME = 0;
  static final int SET_PASSWORD = 4;
  boolean mBound = false;
  private ServiceConnection mConnection = new ServiceConnection()
  {
    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
      SteadyStateCommunicationService.this.mService = new Messenger(paramIBinder);
      SteadyStateCommunicationService.this.mBound = true;
      Message localMessage = new Message();
      localMessage.what = 1;
      try
      {
        SteadyStateCommunicationService.this.mService.send(localMessage);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        localRemoteException.printStackTrace();
      }
    }

    public void onServiceDisconnected(ComponentName paramComponentName)
    {
      SteadyStateCommunicationService.this.mService = null;
      SteadyStateCommunicationService.this.mBound = false;
    }
  };
  final Messenger mMessenger = new Messenger(new IncomingHandler());
  Messenger mService = null;
  private SessionManager smsp;

  public void connectToSteadySTateService()
  {
    Intent localIntent = new Intent("com.edukool.steaystate.OneScommunicationService");
    localIntent.setClassName("com.edukool.steaystate", "com.edukool.steaystate.OneScommunicationService");
    bindService(localIntent, this.mConnection, 1);
  }

  public IBinder onBind(Intent paramIntent)
  {
    return this.mMessenger.getBinder();
  }

  public void onCreate()
  {
    super.onCreate();
    this.smsp = SessionManager.getInstance(this);
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    this.smsp = SessionManager.getInstance(this);
    super.onStart(paramIntent, paramInt);
  }

  class IncomingHandler extends Handler
  {
    IncomingHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
     /* switch (paramMessage.what)
      {
      default:
        super.handleMessage(paramMessage);
      case 0:
      case 1:
      case 2:
      case 3:
      }
      do
      {
        Message localMessage2;
        do
        {
          do
          {
            Utilities localUtilities;
            do
            {
              return;
              localUtilities = new Utilities();
            }
            while ((!SteadyStateCommunicationService.this.smsp.getSpDownloadedMasterSync()) || (localUtilities.isDoViolationCheckOrNot()));
            Intent localIntent = new Intent(SteadyStateCommunicationService.this, HomeScreen.class);
            localIntent.addFlags(268435456);
            SteadyStateCommunicationService.this.startActivity(localIntent);
            SteadyStateCommunicationService.this.smsp.putSpViolationStudentChangeHome(true);
          }
          while (HomeScreen.mTaskHandler == null);
          HomeScreen.mTaskHandler.sendEmptyMessageDelayed(24, 1000L);
          return;
          SteadyStateCommunicationService.this.connectToSteadySTateService();
          return;
          localMessage2 = new Message();
        }
        while (!SteadyStateCommunicationService.this.mBound);
        localMessage2.what = 2;
        try
        {
          SteadyStateCommunicationService.this.mService.send(localMessage2);
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          localRemoteException2.printStackTrace();
          return;
        }
      }
      while ((!SteadyStateCommunicationService.this.mBound) || (!SteadyStateCommunicationService.this.smsp.getSpDownloadedMasterSync()));
      Message localMessage1 = new Message();
      localMessage1.what = 3;
      Bundle localBundle = new Bundle();
      localBundle.putString("isMdm", SteadyStateCommunicationService.this.smsp.getSpMdm());
      localMessage1.setData(localBundle);
      try
      {
        SteadyStateCommunicationService.this.mService.send(localMessage1);
        return;
      }
      catch (RemoteException localRemoteException1)
      {
        localRemoteException1.printStackTrace();
      }*/
    }
  }
}

/* Location:           C:\dex2jar-0.0.9.15\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.dfoeindia.one.master.student.SteadyStateCommunicationService
 * JD-Core Version:    0.6.0
 */