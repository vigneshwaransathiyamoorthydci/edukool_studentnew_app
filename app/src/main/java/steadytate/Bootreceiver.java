package steadytate;

/**
 * Created by kirubakaranj on 6/22/2017.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import Utils.Url;

/**
 *
 * @author André Kullmann
 */
public class Bootreceiver extends BroadcastReceiver {

    /**
     * The password will be set to the childs password. So the device can be unlock by the child.
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("pweb", getClass().getSimpleName() + ".onReceive( " + intent.getAction() + " )");
        Password.get(context).setPassword("").lock();

       // ComponentName admin = new ComponentName( context, SteadyStateDeviceAdminReceiver.class );
        /*Password.get(context)
                .setPassword("");*/

       // Notify.get( context ).showIsReady();
    }

}
