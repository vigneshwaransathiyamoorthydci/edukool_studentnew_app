package steadytate;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;

/**
 * Created by kirubakaranj on 6/22/2017.
 */

public class Password {

    public static Password get( Context context ) {
        return new Password( context );
    }

    private final Context context;

    private Password( Context context ) {
        this.context = context;
    }

    public void lock() {
        DevicePolicyManager dpm = dpm();
        dpm.lockNow();
    }

    public Password setQuality(ComponentName admin, int quality ) {

        dpm().setPasswordQuality(admin, quality);

        return this;
    }

    public Password setPassword( String password ) {

        DevicePolicyManager dpm = dpm();
        dpm.resetPassword(password, DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY );

        return this;
    }

    private DevicePolicyManager dpm() {
        return (DevicePolicyManager) ctx().getSystemService(Context.DEVICE_POLICY_SERVICE);
    }

    private Context ctx() {
        return context;
    }
}
