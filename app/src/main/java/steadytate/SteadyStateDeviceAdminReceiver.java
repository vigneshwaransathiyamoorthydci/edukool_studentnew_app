package steadytate;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class SteadyStateDeviceAdminReceiver extends DeviceAdminReceiver
{
  static final String TAG = "MyLifeCycleHandler";

  public CharSequence onDisableRequested(Context paramContext, Intent paramIntent)
  {

   /* DevicePolicyManager dpm= (DevicePolicyManager) paramContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
    dpm.resetPassword("iyyappa",DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY );*/
    return "Do You want to disable Device Administrator?";
  }

  public void onDisabled(Context paramContext, Intent paramIntent)
  {
    Toast.makeText(paramContext, "Device Admin Disabled", 1).show();
    Log.d("MyLifeCycleHandler", "onDisabled");
  }

  public void onEnabled(Context paramContext, Intent paramIntent)
  {
    super.onEnabled(paramContext, paramIntent);
    Toast.makeText(paramContext, "Device Admin Enabled", 1).show();
    Log.d("MyLifeCycleHandler", "onEnabled");
  }

  public void onPasswordChanged(Context paramContext, Intent paramIntent)
  {
    super.onPasswordChanged(paramContext, paramIntent);
    Log.d("MyLifeCycleHandler", "onPasswordChanged");
  }

  public void onPasswordFailed(Context paramContext, Intent paramIntent)
  {
    super.onPasswordFailed(paramContext, paramIntent);
    Log.d("MyLifeCycleHandler", "onPasswordFailed");
  }

  public void onPasswordSucceeded(Context paramContext, Intent paramIntent)
  {
    super.onPasswordSucceeded(paramContext, paramIntent);
    Log.d("MyLifeCycleHandler", "onPasswordSucceeded");
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent.getAction().equalsIgnoreCase("android.intent.action.BOOT_COMPLETED"))
      Toast.makeText(paramContext, "BOOTING DONE", 2000).show();
    super.onReceive(paramContext, paramIntent);
  }
}

/* Location:           C:\dex2jar-0.0.9.15\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.dfoeindia.one.master.student.SteadyStateDeviceAdminReceiver
 * JD-Core Version:    0.6.0
 */