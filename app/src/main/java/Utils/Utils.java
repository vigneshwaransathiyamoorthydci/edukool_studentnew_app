package Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import adapter.Roomadapter;
import helper.Rooms;
import com.dci.edukool.student.R;

public class Utils {

	Context ctx;
	Activity act;
	Typeface Fontstyle_light,Fontstyle_medium,RobotoCondensed_light;
	SharedPreferences prefer;
	Editor edit;
	//public static String contact="http://gmasa.dci.in/wp-json/contact";

	public Utils(Activity ctxt)
	{
		ctx = ctxt;
		act=ctxt;
		RobotoCondensed_light = Typeface.createFromAsset(act.getAssets(), "RobotoCondensed-Regular.ttf");

       //Fontstyle_light = Typeface.createFromAsset(act.getAssets(), "FUTURALight.TTF");//0
        //Fontstyle_medium= Typeface.createFromAsset(act.getAssets(), "FUTURAMedium.TTF");//1
//Fonstyle_semibold=Typeface.createFromAsset(act.getAssets(), "fonts/OpenSans-Semibold.ttf");//2
		//Fontstyle2 = Typeface.createFromAsset(ctx.getAssets(), "fonts/Swis721MdBTMedium.ttf");
	}
	public boolean hasConnection() {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(
				Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true; 
		}

		return false;
	}


	public void Showalert()
	{

		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("Internet Connection");
		builder.setMessage("Please check your internet connection is available!");
		builder.setPositiveButton("OK", null);
		AlertDialog dialog = builder.show();
		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		TextView titleView = (TextView)dialog.findViewById(ctx.getResources().getIdentifier("alertTitle", "id", "android"));
        if (titleView != null) {
            titleView.setGravity(Gravity.CENTER);
        }
		dialog.show();

	}
	
	public void Showalert1()
	{

		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("App Submitted");
		builder.setMessage("Your App is  Successfully Submitted..");
		builder.setPositiveButton("OK", null);
		AlertDialog dialog = builder.show();
		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		TextView titleView = (TextView)dialog.findViewById(ctx.getResources().getIdentifier("alertTitle", "id", "android"));
        if (titleView != null) {
            titleView.setGravity(Gravity.CENTER);
        }
		dialog.show();

	}

	public void Showexit()
	{
		AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
		builder1.setTitle("KaaKI Sattai");
		builder1.setMessage("Do You Want to exit?");
        builder1.setCancelable(true);
        builder1.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            	
            	act.finish();
                dialog.cancel();
            }
        });
        builder1.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();

	}

	public void setEdittexttypeface(int fno,EditText v)
	{
		/*if(fno==1)
		{
			v.setTypeface(Fontstyle_bold);	
		}*//*else if(fno == 2)
		{
			v.setTypeface(Fontstyle2);	
		}*/
		 if(fno==2)
	{
		v.setTypeface(RobotoCondensed_light);
	}


	}

	public void setTextviewtypeface(int fno,TextView v)
	{
		if(fno == 0)
		{
			v.setTypeface(Fontstyle_light);
		}else if(fno == 1)
		{
			v.setTypeface(Fontstyle_medium);
		}
		else if(fno==2)
		{
			v.setTypeface(RobotoCondensed_light);
		}

	}

	public void setButtontypeface(int fno,Button v)
	{		
		/*if(fno == 0)
		{
			v.setTypeface(Fontstyle_bold);	
		}else if(fno == 1)
		{
			v.setTypeface(Fontstyle_regular);	
		}
		else if(fno==2)
		{
			v.setTypeface(Fonstyle_semibold);
		}*/
	}


	/*public void maskimage(ImageView img,Bitmap bmp)
	{				
		Bitmap mask = BitmapFactory.decodeResource(ctx.getResources(),R.drawable.thumb);
		Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Config.ARGB_8888);		
		bmp = scaleCenterCrop(bmp, mask.getWidth(), mask.getHeight());
		Canvas mCanvas = new Canvas(result);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		mCanvas.drawBitmap(bmp, 0, 0, null);
		mCanvas.drawBitmap(mask, 0, 0, paint);
		paint.setXfermode(null);
		img.setImageBitmap(result);
		img.setScaleType(ScaleType.FIT_XY);
		img.setAdjustViewBounds(true);
		img.setBackgroundResource(R.drawable.thumb);
	}*/


	public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth)
	{
		int sourceWidth = source.getWidth();
		int sourceHeight = source.getHeight();
		float xScale = (float) newWidth / sourceWidth;
		float yScale = (float) newHeight / sourceHeight;
		float scale = Math.max(xScale, yScale);

		//get the resulting size after scaling
		float scaledWidth = scale * sourceWidth;
		float scaledHeight = scale * sourceHeight;

		//figure out where we should translate to
		float dx = (newWidth - scaledWidth) / 2;
		float dy = (newHeight - scaledHeight) / 3;

		Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
		Canvas canvas = new Canvas(dest);
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);
		matrix.postTranslate(dx, dy);
		canvas.drawBitmap(source, matrix, null);
		return dest;
	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}

	/*
	// void showtoast(String message)
	{
		Toast.makeText(act, message, 1000).show();
	}*/
	
	
	public void Showalert2()
	 {

	  AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
	  builder.setTitle("Submitted");
	  builder.setMessage("Your Form is Successfully Submitted..");
	  builder.setPositiveButton("OK", null);
	  AlertDialog dialog = builder.show();
	  TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
	  messageText.setGravity(Gravity.CENTER);
	  TextView titleView = (TextView)dialog.findViewById(ctx.getResources().getIdentifier("alertTitle", "id", "android"));
	        if (titleView != null) {
	            titleView.setGravity(Gravity.CENTER);
	        }
	  dialog.show();

	 }
	 public String getLocalIpAddress() {
	     try {
	         for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
	             NetworkInterface intf = en.nextElement();
	             for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
	                 InetAddress inetAddress = enumIpAddr.nextElement();
	                 if (!inetAddress.isLoopbackAddress()) {
	                     @SuppressWarnings("deprecation")
                         String ip = Formatter.formatIpAddress(inetAddress.hashCode());
	                     Log.e("***** IP=", ip);
	                     return ip;
	                 }
	             }
	         }
	     } catch (SocketException ex) {
	         Log.e("***** IP=", ex.toString());
	     }
	     return null;
	 }
/*public void ratethisapp()
{
	final Dialog dialog = new Dialog(ctx);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setContentView(R.layout.customdialog);
	dialog.setTitle("RATE...");

 Button rate=(Button) dialog.findViewById(R.id.yes);
 Button later=(Button) dialog.findViewById(R.id.no);
 Button never=(Button) dialog.findViewById(R.id.never);
 
 
 rate.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		   Intent intent = new Intent(Intent.ACTION_VIEW);
           intent.setData(Uri.parse
("market://details?id=com.dci.kakkisattai"));
         ctx.startActivity(intent);
         dialog.dismiss();
        
         
	}
});
 
 later.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
     edit.putInt("inc", 1);
     edit.commit();
     dialog.dismiss();
		
	}
});
 never.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		 edit.putBoolean("never", true);
	     edit.commit();
	     dialog.dismiss();
	     
		
	}
});*/
	
	// set the custom dialog components - text, image and button
	/*TextView text = (TextView) dialog.findViewById(R.id.text);
	text.setText("Android custom dialog example!");
	ImageView image = (ImageView) dialog.findViewById(R.id.image);
	image.setImageResource(R.drawable.ic_launcher);

	Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
	// if button is clicked, close the custom dialog
	dialogButton.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			dialog.dismiss();
		}
	});*/

	/*dialog.show();
}*/
	 
	 
	 public void Showalert3()
	 {

	  AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
	  builder.setTitle("Submitted");
	  builder.setMessage("Thank you. We have received your registration details for GMASA and only the payment process is pending. A member of our team will be contacting you shortly for the same.");
	  builder.setPositiveButton("OK", null);
	  AlertDialog dialog = builder.show();
	  TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
	  messageText.setGravity(Gravity.CENTER);
	  TextView titleView = (TextView)dialog.findViewById(ctx.getResources().getIdentifier("alertTitle", "id", "android"));
	        if (titleView != null) {
	            titleView.setGravity(Gravity.CENTER);
	        }
	  dialog.show();

	 }
public boolean readfile(String search)
{
boolean check;
    BufferedReader reader = null;
    try {
        reader = new BufferedReader(
                new InputStreamReader(act.getAssets().open("AdultWords.csv")));

        // do reading, usually loop until end of file reading
        String mLine;
        int i=0;
        while ((mLine = reader.readLine()) != null) {
            //process line
          if(mLine.contains(search))
          {
              Log.v("" + i, mLine);
           return true;

          }
            else {
              Log.v("" + i, mLine);

          }
           // if(check)
        }
    } catch (IOException e) {
        //log the exception
    } finally {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                //log the exception
            }
        }
       // return false;
    }
    return false;
}

public ArrayList<String> datefunction(String getdate) {
  //  DateTimeUtils obj = new DateTimeUtils();
   /* SimpleDateFormat simpleDateFormat =
            new SimpleDateFormat("dd/M/yyyy hh:mm:ss");*/
    SimpleDateFormat format = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    format.setTimeZone(TimeZone.getTimeZone("UTC"));

    try {

        Date date1 = format.parse(getdate);
        Date date2 = format.parse(format.format(new Date()));

      return printDifference(date1, date2);

    } catch (ParseException e) {
        e.printStackTrace();
    }
        return null;

}



    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public ArrayList<String> printDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);


       // Long month=elapsedDays/30;
        ArrayList<String> getdate=new ArrayList<>();
        getdate.add(""+getDiffYears(startDate,endDate));

                 getdate.add(""+elapsedDays);
                 getdate.add(""+elapsedHours);
        getdate.add(""+elapsedMinutes);
        getdate.add(""+elapsedSeconds);
        Long month=elapsedDays/30;
        getdate.add(""+month);




        return  getdate;

    }


    public  int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }



    public String convertYouTubeDuration(String duration) {
        String youtubeDuration = duration; //"PT1H2M30S"; // "PT1M13S";
        Calendar c = new GregorianCalendar();
        try {
            DateFormat df = new SimpleDateFormat("'PT'mm'M'ss'S'");
            Date d = df.parse(youtubeDuration);
            c.setTime(d);
        } catch (ParseException e) {
            try {
                DateFormat df = new SimpleDateFormat("'PT'hh'H'mm'M'ss'S'");
                Date d = df.parse(youtubeDuration);
                c.setTime(d);
            } catch (ParseException e1) {
                try {
                    DateFormat df = new SimpleDateFormat("'PT'ss'S'");
                    Date d = df.parse(youtubeDuration);
                    c.setTime(d);
                } catch (ParseException e2) {
                    try {
                        DateFormat df = new SimpleDateFormat("'PT'mm'M'");
                        Date d = df.parse(youtubeDuration);
                        c.setTime(d);
                    }
                    catch (ParseException e3)
                    {
                        try {
                            DateFormat df = new SimpleDateFormat("'PT'hh'H'");
                            Date d = df.parse(youtubeDuration);
                            c.setTime(d);
                        }
                        catch (ParseException e4)
                        {
                            try {
                                DateFormat df = new SimpleDateFormat("'PT'hh'H''mm'M'");
                                Date d = df.parse(youtubeDuration);
                                c.setTime(d);
                            }
                            catch (ParseException e5)
                            {

                            }
                        }
                    }
                }
            }
        }
        c.setTimeZone(TimeZone.getDefault());

        String time = "";
        if ( c.get(Calendar.HOUR) > 0 ) {
            if ( String.valueOf(c.get(Calendar.HOUR)).length() == 1 ) {
                time += "0" + c.get(Calendar.HOUR);
            }
            else {
                time += c.get(Calendar.HOUR);
            }
            time += ":";
        }
        // test minute
        if ( String.valueOf(c.get(Calendar.MINUTE)).length() == 1 ) {
            time += "0" + c.get(Calendar.MINUTE);
        }
        else {
            time += c.get(Calendar.MINUTE);
        }
        time += ":";
        // test second
        if ( String.valueOf(c.get(Calendar.SECOND)).length() == 1 ) {
            time += "0" + c.get(Calendar.SECOND);
        }
        else {
            time += c.get(Calendar.SECOND);
        }
        return time ;
    }


	public static void hideSoftKeyboard(Activity activity) {
		InputMethodManager inputMethodManager =
				(InputMethodManager) activity.getSystemService(
						Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(
				activity.getCurrentFocus().getWindowToken(), 0);
	}

    public static void lockpoopup(Activity act,String heading,String text)
    {
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.deviceunlock);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView textheading= (TextView) dialog.findViewById(R.id.textView2);
        TextView content= (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close= (ImageView) dialog.findViewById(R.id.close);
        textheading.setText(heading);
        content.setText(text);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }
    public static void listofrooms(Activity act,ArrayList<Rooms>room){

        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.handraiselayoutfile);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setCancelable(false);




  /*View popUpView =act.getLayoutInflater().inflate(R.layout.handraiselayoutfile,
    null); // inflating popup layout
  final PopupWindow mpopup = new PopupWindow(popUpView, RelativeLayout.LayoutParams.FILL_PARENT,
    RelativeLayout.LayoutParams.WRAP_CONTENT, true); // Creation of popup
  mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
  mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);*/
  /*LayoutInflater inflater = (LayoutInflater) act
    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  View layout = inflater.inflate(R.layout.handraiselayoutfile,null);*/

        ListView listView;
        ImageView close;
        TextView text;
        ImageView image;

        listView=(ListView)dialog.findViewById(R.id.handriselistvie);
        image= (ImageView) dialog.findViewById(R.id.boyhand);
        text= (TextView) dialog.findViewById(R.id.handraiseheading);

        close= (ImageView) dialog.findViewById(R.id.close);
        text.setText("ROOMS");
        image.setImageResource(R.drawable.rooms);
        if(room.size()>0)
        {
            Roomadapter handraiseadapter=new Roomadapter(act,room);
            listView.setAdapter(handraiseadapter);

            //new Handraiseconnectin(LoginActivity.handraise,"Ack").start();


        }

        //mpopup.show();

        //adapter = new UsersAdapter(act, );
        //listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        //doInback();


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // LoginActivity.handraise.clear();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

