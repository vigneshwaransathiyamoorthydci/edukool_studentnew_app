package com.dci.edukool.student;

/**
 * Created by kirubakaranj on 5/5/2017.
 */

public class SqliteOpen {

    //for school table
    String Schoolid;
    String SchoolName;
    String Acronym;
    String SchoolLogo;
    String backgroundimage;

    public String getBackgroundimage() {
        return backgroundimage;
    }

    public void setBackgroundimage(String backgroundimage) {
        this.backgroundimage = backgroundimage;
    }


    public String getSchoolid() {
        return Schoolid;
    }

    public void setSchoolid(String schoolid) {
        Schoolid = schoolid;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getAcronym() {
        return Acronym;
    }

    public void setAcronym(String acronym) {
        Acronym = acronym;
    }

    public String getSchoolLogo() {
        return SchoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        SchoolLogo = schoolLogo;
    }






    //for class table
    String ClassID,ClassName,SectionName,Grade,SchoolID,DepartmentID,InternetSSID,InternetPassword,InternetType,PushName;
    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(String schoolID) {
        SchoolID = schoolID;
    }

    public String getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(String departmentID) {
        DepartmentID = departmentID;
    }

    public String getInternetSSID() {
        return InternetSSID;
    }

    public void setInternetSSID(String internetSSID) {
        InternetSSID = internetSSID;
    }

    public String getInternetPassword() {
        return InternetPassword;
    }

    public void setInternetPassword(String internetPassword) {
        InternetPassword = internetPassword;
    }

    public String getInternetType() {
        return InternetType;
    }

    public void setInternetType(String internetType) {
        InternetType = internetType;
    }

    public String getPushName() {
        return PushName;
    }

    public void setPushName(String pushName) {
        PushName = pushName;
    }






}
