package com.dci.edukool.student;

/**
 * Created by kirubakaranj on 5/8/2017.
 */

public class Staffpojo {
    String Staffid;
    String DOJ;
    String FirstName;
    String LastName;
    String Gender;
    String DOB;
    String MaritalStatusID;
    String SpouseName;
    String FatherName;
    String MotherName;
    String Phone;
    String Email;
    String PhotoFilename;
    String StaffCategoryID;

    public String getStaffid() {
        return Staffid;
    }

    public void setStaffid(String staffid) {
        Staffid = staffid;
    }

    public String getDOJ() {
        return DOJ;
    }

    public void setDOJ(String DOJ) {
        this.DOJ = DOJ;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMaritalStatusID() {
        return MaritalStatusID;
    }

    public void setMaritalStatusID(String maritalStatusID) {
        MaritalStatusID = maritalStatusID;
    }

    public String getSpouseName() {
        return SpouseName;
    }

    public void setSpouseName(String spouseName) {
        SpouseName = spouseName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhotoFilename() {
        return PhotoFilename;
    }

    public void setPhotoFilename(String photoFilename) {
        PhotoFilename = photoFilename;
    }

    public String getStaffCategoryID() {
        return StaffCategoryID;
    }

    public void setStaffCategoryID(String staffCategoryID) {
        StaffCategoryID = staffCategoryID;
    }



}
