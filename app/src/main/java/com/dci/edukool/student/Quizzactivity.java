package com.dci.edukool.student;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import connection.Client;

/**
 * Created by abimathi on 30-May-17.
 */
public class Quizzactivity extends Activity {
     TextView timer;
    ImageView close;
     RelativeLayout bottom;
     TextView newquizz;
    // ImageView start;
     //ImageView reset;
    TextView timertext2;
    TextView timertext3;
    ImageView img_reset;
    ImageView img_start;
    BroadcastReceiver startquizz;
    CountDownTimer timercount;
    Client mClient;
    //Buzzerclass buzz=null;
    BroadcastReceiver closealert;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    SqliteOpenHelperDemo obj;
    ImageView buzz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.quizz);

        pref=getSharedPreferences("student",MODE_PRIVATE);
        edit=pref.edit();
        obj = new SqliteOpenHelperDemo(getApplicationContext());
        this.setFinishOnTouchOutside(false);
        timer= (TextView) findViewById(R.id.timertext);
        timertext2= (TextView) findViewById(R.id.timertext2);
        timertext3= (TextView) findViewById(R.id.timertext3);
        img_reset= (ImageView) findViewById(R.id.resetimage);
        img_start= (ImageView) findViewById(R.id.startimage);
        close = (ImageView) findViewById(R.id.close);
        bottom= (RelativeLayout) findViewById(R.id.bottomlayout);
        newquizz = (TextView) findViewById(R.id.newquiz);
        buzz= (ImageView) findViewById(R.id.buzzer);
       // start = (ImageView) findViewById(R.id.start);
     //  reset= (ImageView) findViewById(R.id.reset);
        img_start.setEnabled(false);
        img_reset.setEnabled(false);
        bottom.setEnabled(false);

        timercreate();
        edit.putBoolean("quizz", false);
        edit.commit();

       /* close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        timercount.start();


        newquizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newquizz.setEnabled(false);
                img_start.setEnabled(true);
                img_reset.setEnabled(true);
                bottom.setEnabled(true);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
buzz.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        new connectTask().execute();
        finish();
    }
});


        img_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //tratExamCompletedPopup("");
               // img_start.setEnabled(false);


           /*   timercount=  new CountDownTimer(35000, 1) {

                    public void onTick(long millisUntilFinished) {
                        //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
                        //here you can have your logic to set text to edittext
                        timer.setText("" + millisUntilFinished / 1000);
                        timertext3.setText("" + millisUntilFinished % 1000);


                    }

                    public void onFinish() {
                        img_start.setEnabled(true);
                        timer.setText("00");
                        timertext3.setText("00");
                        //finish();
                    }

                }.start();*/


            }
        });
        img_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timercount.cancel();
                img_start.setEnabled(true);
                timer.setText("00");
                timertext3.setText("00");
                String senddata="Quiz@reset";
               //ew clientThread(MultiThreadChatServerSync.thread,senddata).start();

            }
        });
        bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String senddata="Quiz@reset";
                /// clientThread(MultiThreadChatServerSync.thread,senddata).start();

                //finish();
            }
        });


/*
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///dialog.dismiss();
            }
        });*/
    }


    @Override
    protected void onResume() {
        super.onResume();
        startquizz=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                img_start.setEnabled(false);

              /*  if(buzz==null)
                {
                    buzz=new Buzzerclass();
                }
                else
                {
                    try
                    {
                        buzz.closesocket();
                        buzz=new Buzzerclass();
                    }
                    catch (Exception e)
                    {
                        buzz=new Buzzerclass();
                    }
                }*/


            }
        };
        IntentFilter quizfilter=new IntentFilter("quizz");
        registerReceiver(startquizz,quizfilter);

        closealert=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                finish();
            }
        };
        IntentFilter fileter=new IntentFilter("closealert");
        registerReceiver(closealert,fileter);
    }
void timercreate()
{

    timercount=  new CountDownTimer(15000, 1) {

        public void onTick(long millisUntilFinished) {
            //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
            //here you can have your logic to set text to edittext
            timer.setText("" + millisUntilFinished / 1000);
            timertext3.setText("" + millisUntilFinished % 1000);


        }

        public void onFinish() {
            img_start.setEnabled(true);
            timer.setText("00");
            timertext3.setText("00");
            finish();
           // Toast.makeText(Quizzactivity.this,"No one has press buzzer",Toast.LENGTH_SHORT).show();
            //finish();
        }

    };
}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(startquizz);
        unregisterReceiver(closealert);
    }

    public class connectTask extends AsyncTask<String,String,Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and

            String rollno="",schoolid="",stdid="",fname="";
            Cursor stdcursor=obj.retrive("tblStudent");
            while(stdcursor.moveToNext()){
                rollno= stdcursor.getString(stdcursor.getColumnIndex("RollNo"));
                schoolid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("SchoolID")));
                stdid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("StudentID")));
                fname= stdcursor.getString(stdcursor.getColumnIndex("FirstName"));

            }

            String s="buzz@"+stdid+"@"+rollno;
           // String s="invS@connect@"+getIpAddress()+"@"+rollno+"@"+schoolid+"@"+fname+"@"+stdid+"@"+currentdate1();
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            },pref,s);
            mClient.run();

            return null;
        }

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }
}
