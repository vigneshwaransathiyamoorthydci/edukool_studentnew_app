package com.dci.edukool.student;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Method;

/**
 * Created by abimathi on 12-May-17.
 */
public class Devicelock extends Activity {

    public static boolean devicelock;
    BroadcastReceiver unlock;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    public static boolean backpressed;
    public static Activity devicelockact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.devicelocked);
        pref = getSharedPreferences("student", MODE_PRIVATE);
        edit = pref.edit();
        edit.putBoolean("devicelock",true);
        edit.commit();
        devicelockact=Devicelock.this;
        devicelock=true;
        unlock=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                devicelock=false;
                edit.putBoolean("devicelock",false);
                edit.commit();
                finish();

            }
        };
        IntentFilter filter=new IntentFilter("unlock");
        registerReceiver(unlock,filter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        try
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        catch (Exception e)
        {

        }


    }

    @Override
    protected void onPause() {
        super.onPause();
       //
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(unlock);
        }
        catch (Exception e)
        {

        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        if(backpressed)
            super.onBackPressed();

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
