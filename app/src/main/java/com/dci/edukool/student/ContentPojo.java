package com.dci.edukool.student;

/**
 * Created by kirubakaranj on 5/8/2017.
 */

public class ContentPojo {

    String ContentDescription;
    String ContentType;
    String ContentFilename;
    String Vaporize;
    String contentid;
    String Subject;
    String Catalog;

    public String getCatalog() {
        return Catalog;
    }

    public void setCatalog(String catalog) {
        Catalog = catalog;
    }



    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }



    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }


    public String getContentDescription() {
        return ContentDescription;
    }

    public void setContentDescription(String contentDescription) {
        ContentDescription = contentDescription;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String contentType) {
        ContentType = contentType;
    }

    public String getContentFilename() {
        return ContentFilename;
    }

    public void setContentFilename(String contentFilename) {
        ContentFilename = contentFilename;
    }

    public String getVaporize() {
        return Vaporize;
    }

    public void setVaporize(String vaporize) {
        Vaporize = vaporize;
    }




}
