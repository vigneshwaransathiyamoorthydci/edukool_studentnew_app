package com.dci.edukool.student;

/**
 * Created by kirubakaranj on 6/7/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import Utils.Utils;
/**
 * Created by iyyapparajr on 5/8/2017.
 */


/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Subspinner extends ArrayAdapter {


    Utils utils;
    private Context context;
    private ArrayList<Masterpojo> sublist;
    Activity act;
    public Subspinner(Context context, int textViewResourceId,ArrayList<Masterpojo> sublist) {

        super(context, textViewResourceId,sublist);
        this.context=context;
        act= (Activity) context;
        this.sublist=sublist;
        utils=new Utils(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(sublist.get(position).getSubjectname());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        utils.setTextviewtypeface(2,make);

        make.setText(sublist.get(position).getSubjectname());

        return row;
    }

}
