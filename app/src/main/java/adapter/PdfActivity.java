package adapter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;

import org.ebookdroid.common.touch.TouchManagerView;
import org.ebookdroid.ui.viewer.IView;
import org.ebookdroid.ui.viewer.ViewerActivity;
import org.ebookdroid.ui.viewer.ViewerActivityController;
import org.ebookdroid.ui.viewer.views.ManualCropView;
import org.ebookdroid.ui.viewer.views.PageViewZoomControls;
import org.ebookdroid.ui.viewer.views.SearchControls;
import org.emdev.common.log.LogContext;
import org.emdev.ui.AbstractActionActivity;
import org.emdev.ui.uimanager.IUIManager;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import Utils.Utils;
import connection.Client;

public class PdfActivity extends Activity implements OnPageChangeListener,Loader.OnLoadCompleteListener, OnLoadCompleteListener {
    private static final String TAG = PdfActivity.class.getSimpleName();
    public static String SAMPLE_FILE = "/storage/emulated/0/csr_39/Users/S_39/Material/Academic/Std12-Tamil.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    public static final DisplayMetrics DM = new DisplayMetrics();

    private static final AtomicLong SEQ = new AtomicLong();



    IView view;

    private Toast pageNumberToast;

    private Toast zoomToast;

    private PageViewZoomControls zoomControls;

    private SearchControls searchControls;

    private FrameLayout frameLayout;

    private TouchManagerView touchView;

    private boolean menuClosedCalled;

    private ManualCropView cropControls;



    TextView pagetext;
    boolean lock=false;
    int page;

    Bundle savedinstance;
    boolean syn;
    BroadcastReceiver forhandrais;

    String sendata;
    private ImageView bookbinback;
    private ImageView bookbinhandrise;
    BroadcastReceiver movepage;

    private ImageView bookbinblock;
    private ImageView bookbinarrow;

    private ImageView bookbinprojection;
    private ImageView bookbinorientation;
    private ImageView bookbinpulse;
    private ImageView bookbinsync;
    private ImageView bookbinlock;
    SharedPreferences pref;
    Client mClient;
    public static Activity pdfact=null;
    int from;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        pdfView= (PDFView)findViewById(R.id.pdfView);
        pagetext= (TextView) findViewById(R.id.pagetext);
        bookbinback= (ImageView) findViewById(R.id.bookbinback);
        bookbinhandrise= (ImageView) findViewById(R.id.handrise);
        bookbinblock= (ImageView) findViewById(R.id.block);
        bookbinarrow= (ImageView) findViewById(R.id.bookbinarrow);
        bookbinprojection=(ImageView) findViewById(R.id.projector);
        bookbinorientation= (ImageView) findViewById(R.id.orientation);
        bookbinpulse= (ImageView) findViewById(R.id.pulse);
        bookbinsync= (ImageView) findViewById(R.id.sync);
        bookbinlock= (ImageView) findViewById(R.id.lock);
        pref = getSharedPreferences("student", MODE_PRIVATE);
        SAMPLE_FILE= getIntent().getStringExtra("bookname");
        from=getIntent().getIntExtra("from",2);
        lock=getIntent().getBooleanExtra("lock",false);
        Log.d("VIKISSS", "path" + SAMPLE_FILE);
        if (from==2) {
            SAMPLE_FILE = SAMPLE_FILE.substring(6);
            displayFromAsset(SAMPLE_FILE);

        }
        else
        {
            pageNumber= Integer.valueOf(getIntent().getStringExtra("pagenumber"));
            displayFromAsset(SAMPLE_FILE);


        }
        //pagetext.setText(getString(pdfView.getCurrentPage()));
        bookbinback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent in=new Intent("filemove");
                //  in.putExtra("page",file[1].toString());
                try {
                    in.putExtra("close", true);
                    sendBroadcast(in);
                }
                catch (Exception e)
                {

                }*/

                  finish();

            }
        });
        bookbinhandrise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!pref.getBoolean("hand",false)) {
                    //pdfView.jumpTo(5);
                    new connectTask().execute();
                    if(pref.getBoolean("classtime",false))
                    {
                        Toast.makeText(PdfActivity.this,"Handraise sent successfully",Toast.LENGTH_SHORT).show();

                    }
                }else
                {
                    Utils.lockpoopup(PdfActivity.this,"Handraise","Your teacher has blocked the handraise capability.");
                }
            }
        });

    }



    public class connectTask extends android.os.AsyncTask<String,String, Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and
            SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(PdfActivity.this);
            String rollno="",schoolid="",stdid="",fname="";
            Cursor stdcursor=obj.retrive("tblStudent");
            while(stdcursor.moveToNext()){
                rollno= stdcursor.getString(stdcursor.getColumnIndex("RollNo"));
                schoolid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("SchoolID")));
                stdid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("StudentID")));
                fname= stdcursor.getString(stdcursor.getColumnIndex("FirstName"));

            }
            String s="";
            String stringToSend = "Signal@" + fname + "@"+stdid+"@" +rollno+"@"+pref.getString("ip","");
            //String s="invS@Connect@IPAddress@RollNumber";
            //	String s="invS@connect@"+getIpAddress()+"@"+rollno+"@"+schoolid+"@"+fname+"@"+stdid+"@"+currentdate1();
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            },pref,stringToSend);
            mClient.run();

            return null;
        }

    }

    private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;
        File file=new File(SAMPLE_FILE);
        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();



    }


    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
        pagetext.setText(String.valueOf(page+1)+"/"+String.valueOf(pageCount));

    }




    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onLoadComplete(Loader loader, Object data) {

    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

}