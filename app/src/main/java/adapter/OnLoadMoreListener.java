package adapter;

public interface OnLoadMoreListener {
    void onLoadMore();
}