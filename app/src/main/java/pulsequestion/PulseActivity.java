package pulsequestion;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import connection.Client;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

import java.lang.reflect.Method;

/**
 * Created by pratheeba on 5/29/2017.
 */



public class PulseActivity extends Activity {
    JSONArray options;
    String studentResponse;
    SharedPreferences.Editor edit;
    SharedPreferences pref;
    CountDownTimer pulse;
    int PulseQuestionId;
    Client mClient=null;
    int StudentID;
    TextView timer, questionTxt;
    Button option1,option2,option3,option4;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.pulse_ques_lay);
        pref=getSharedPreferences("student",MODE_PRIVATE);
        edit=pref.edit();
        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());
        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            StudentID= tc.getInt(tc.getColumnIndex("StudentID"));
        }
        timer = (TextView)findViewById(R.id.timer);
        questionTxt= (TextView)findViewById(R.id.questiontxt);
        option1= (Button)findViewById(R.id.option1);
        option2= (Button)findViewById(R.id.option2);
        option3= (Button)findViewById(R.id.option3);
        option4= (Button)findViewById(R.id.option4);

        pulse =  new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("00:00:"+millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                String parent = "PulseAnswer"+"@"+PulseQuestionId+"@"+"5"+"@"+StudentID;
                studentResponse = parent.toString();
                new connectTask().execute();
                timer.setText("done!");
                finish();
            }

        }.start();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String Pulsequestion= bundle.getString("Pulsequestion");


        String pulse_split [] = Pulsequestion.split("\\@");

        try {
            JSONObject array = new JSONObject(pulse_split[1]);
              if(array.getString("PulseQuestionId").equalsIgnoreCase("")){
                  PulseQuestionId = 0;
              }
            else{
                  PulseQuestionId = array.getInt("PulseQuestionId");
              }
            String PulseQuestion = array.getString("PulseQuestion");
            questionTxt.setText(PulseQuestion);

            options = array.getJSONArray("Options");
            if(options.length()==1){
                option1.setText(options.get(0).toString());
                option2.setVisibility(View.GONE);
                option3.setVisibility(View.GONE);
                option4.setVisibility(View.GONE);

            }
            else if(options.length()==2){
                option1.setText(options.get(0).toString());
                option2.setText(options.get(1).toString());
                option3.setVisibility(View.GONE);
                option4.setVisibility(View.GONE);


            }
            else if (options.length()==3){
                option1.setText(options.get(0).toString());
                option2.setText(options.get(1).toString());
                option3.setText(options.get(2).toString());
                option4.setVisibility(View.GONE);

            }
            else  if (options.length()==4){
                option1.setText(options.get(0).toString());
                option2.setText(options.get(1).toString());
                option3.setText(options.get(2).toString());
                option4.setText(options.get(3).toString());

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parent = "PulseAnswer"+"@"+PulseQuestionId+"@"+"1"+"@"+StudentID;
                studentResponse = parent.toString();
                pulse.cancel();
                new connectTask().execute();

                finish();

            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parent = "PulseAnswer"+"@"+PulseQuestionId+"@"+"2"+"@"+StudentID;
                studentResponse = parent.toString();
                pulse.cancel();
                new connectTask().execute();
                finish();

            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parent = "PulseAnswer"+"@"+PulseQuestionId+"@"+"3"+"@"+StudentID;
                studentResponse = parent.toString();
                pulse.cancel();
                new connectTask().execute();
                finish();

            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parent = "PulseAnswer" + "@" + PulseQuestionId + "@" + "4" + "@" + StudentID;
                studentResponse = parent.toString();
                pulse.cancel();
                new connectTask().execute();

                finish();

            }
        });




    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onResume();
    }

    public class connectTask extends AsyncTask<String,String, Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            },pref,studentResponse);
            mClient.run();

            return null;
        }

    }

    public void startTimer(final long finish, long tick)
    {
        new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished)
            {
                long remainedSecs = millisUntilFinished/1000;
                timer.setText(""+("00:"+remainedSecs/60)+":"+(remainedSecs%60));// manage it accordign to you
                // Utils.setTextviewtypeface(2, timer_text);

            }

            public void onFinish()
            {
                timer.setText("00:00:00");
                //  cancel();
                finish();


                Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_LONG).show();
                timer.setText("done!");
                // Utils.setTextviewtypeface(2, timer_text);

            }
        }.start();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
