package org.ebookdroid.ui.viewer;

import org.ebookdroid.EBookDroidApp;

import connection.Client;
import com.dci.edukool.student.R;
import org.ebookdroid.common.settings.AppSettings;
import org.ebookdroid.common.settings.books.BookSettings;
import org.ebookdroid.common.settings.books.Bookmark;
import org.ebookdroid.common.settings.types.ToastPosition;
import org.ebookdroid.common.touch.TouchManagerView;
import org.ebookdroid.ui.viewer.views.ManualCropView;
import org.ebookdroid.ui.viewer.views.PageViewZoomControls;
import org.ebookdroid.ui.viewer.views.SearchControls;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicLong;

import org.emdev.common.log.LogContext;
import org.emdev.common.log.LogManager;
import org.emdev.ui.AbstractActionActivity;
import org.emdev.ui.uimanager.IUIManager;
import org.emdev.utils.LengthUtils;

import Utils.Utils;
import drawboard.Whiteboard;

import com.dci.edukool.student.SqliteOpenHelperDemo;


/*@ActionTarget(
// action list
actions = {
// start
@ActionMethodDef(id = R.id.mainmenu_about, method = "showAbout")
// finish
})*/
public class ViewerActivity extends AbstractActionActivity<ViewerActivity, ViewerActivityController> {

    public static final DisplayMetrics DM = new DisplayMetrics();

    private static final AtomicLong SEQ = new AtomicLong();

    final LogContext LCTX;

    IView view;

    private Toast pageNumberToast;

    private Toast zoomToast;

    private PageViewZoomControls zoomControls;

    private SearchControls searchControls;

    private FrameLayout frameLayout;

    private TouchManagerView touchView;

    private boolean menuClosedCalled;

    private ManualCropView cropControls;



    TextView pagetext;
    boolean lock;
    int page;

    Bundle savedinstance;
    boolean syn;
    BroadcastReceiver forhandrais;

    String sendata;
    private ImageView bookbinback;
    private ImageView bookbinhandrise;
    BroadcastReceiver movepage;

    private ImageView bookbinblock;
    private ImageView bookbinarrow;

    private ImageView bookbinprojection;
    private ImageView bookbinorientation;
    private ImageView bookbinpulse;
    private ImageView bookbinsync;
    private ImageView bookbinlock;
    SharedPreferences pref;
    Client mClient;
    public static Activity pdfact=null;
    /**
     * Instantiates a new base viewer activity.
     */
    public ViewerActivity() {
        super();
        LCTX = LogManager.root().lctx(this.getClass().getSimpleName(), true).lctx("" + SEQ.getAndIncrement(), true);

    }

    /**
     * {@inheritDoc}
     *
     * @see AbstractActionActivity#createController()
     */
    @Override
    protected ViewerActivityController createController() {
        return new ViewerActivityController(this);
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onNewIntent(): " + intent);

        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {


        if (LCTX.isDebugEnabled()) {
            LCTX.d("onCreate(): " + getIntent());
        }

        restoreController();
        getController().beforeCreate(this);

        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/

        getWindowManager().getDefaultDisplay().getMetrics(DM);
        LCTX.i("XDPI=" + DM.xdpi + ", YDPI=" + DM.ydpi);

       // frameLayout = new FrameLayout(this);

        view = AppSettings.current().viewType.create(getController());
        this.registerForContextMenu(view.getView());
        pdfact=ViewerActivity.this;


      //  LayoutUtils.fillInParent(frameLayout, view.getView());

      /*  frameLayout.addView(view.getView());
        frameLayout.addView(getZoomControls());
        frameLayout.addView(getManualCropControls());
        frameLayout.addView(getSearchControls());
        frameLayout.addView(getTouchView());*/

        getController().afterCreate();

        setContentView(R.layout.newpdflayout);


        pagetext= (TextView) findViewById(R.id.pagetext);

        frameLayout= (FrameLayout) findViewById(R.id.frame);
        frameLayout.addView(view.getView());
        frameLayout.addView(getZoomControls());
        frameLayout.addView(getManualCropControls());
        frameLayout.addView(getSearchControls());
        frameLayout.addView(getTouchView());
        pref = getSharedPreferences("student", MODE_PRIVATE);

        int pageNumber=9;

        final int pageCount = getController().getDocumentModel().getPageCount();
        if (pageNumber > 0 && pageNumber < pageCount) {
          //  final String msg = base.getContext().getString(R.string.bookmark_invalid_page, offset, pageCount - 1 + offset);
            //Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
           // return false;

        }
       // getController().jumpToPage(pageNumber, 0, 0, AppSettings.current().storeGotoHistory);

        bookbinback= (ImageView) findViewById(R.id.bookbinback);
        bookbinhandrise= (ImageView) findViewById(R.id.handrise);
        bookbinblock= (ImageView) findViewById(R.id.block);

        bookbinarrow= (ImageView) findViewById(R.id.bookbinarrow);
        bookbinprojection=(ImageView) findViewById(R.id.projector);
        bookbinorientation= (ImageView) findViewById(R.id.orientation);
        bookbinpulse= (ImageView) findViewById(R.id.pulse);
        bookbinsync= (ImageView) findViewById(R.id.sync);
        bookbinlock= (ImageView) findViewById(R.id.lock);
       /* bookbinback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/

       if(ViewerActivityController.lock)
       {
           bookbinback.setEnabled(false);
       }
       else
       {
           bookbinback.setEnabled(true);

       }

        bookbinback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent in=new Intent("filemove");
                //  in.putExtra("page",file[1].toString());
                try {
                    in.putExtra("close", true);
                    sendBroadcast(in);
                }
                catch (Exception e)
                {

                }*/
                finish();
            }
        });
        bookbinhandrise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!pref.getBoolean("hand",false)) {
                    new  connectTask().execute();
                    if(pref.getBoolean("classtime",false))
                    {
                        Toast.makeText(ViewerActivity.this,"Handraise sent successfully",Toast.LENGTH_SHORT).show();

                    }
                }else
                {
                    Utils.lockpoopup(ViewerActivity.this,"Handraise","Your teacher has blocked the handraise capability.");
                }
            }
        });


    }
    public class connectTask extends android.os.AsyncTask<String,String, Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and
            SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(ViewerActivity.this);
            String rollno="",schoolid="",stdid="",fname="";
            Cursor stdcursor=obj.retrive("tblStudent");
            while(stdcursor.moveToNext()){
                rollno= stdcursor.getString(stdcursor.getColumnIndex("RollNo"));
                schoolid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("SchoolID")));
                stdid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("StudentID")));
                fname= stdcursor.getString(stdcursor.getColumnIndex("FirstName"));

            }
            String s="";
            String stringToSend = "Signal@" + fname + "@"+stdid+"@" +rollno+"@"+pref.getString("ip","");
            //String s="invS@Connect@IPAddress@RollNumber";
            //	String s="invS@connect@"+getIpAddress()+"@"+rollno+"@"+schoolid+"@"+fname+"@"+stdid+"@"+currentdate1();
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            },pref,stringToSend);
            mClient.run();

            return null;
        }

    }

    @Override
    protected void onResume() {
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onResume()");
        }

        getController().beforeResume();

        super.onResume();
        IUIManager.instance.onResume(this);


        getController().afterResume();
    }

    @Override
    protected void onPause() {
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onPause(): " + isFinishing());
        }

        getController().beforePause();

        super.onPause();
        IUIManager.instance.onPause(this);

        getController().afterPause();
    }

    @Override
    public void onWindowFocusChanged(final boolean hasFocus) {
        if (hasFocus && this.view != null) {
            IUIManager.instance.setFullScreenMode(this, this.view.getView(), AppSettings.current().fullScreen);
        }
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        final boolean finishing = isFinishing();
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onDestroy(): " + finishing);
        }

        getController().beforeDestroy(finishing);
        super.onDestroy();
        getController().afterDestroy(finishing);

        EBookDroidApp.onActivityClose(finishing);
    }

    protected IView createView() {
        return AppSettings.current().viewType.create(getController());
    }

    public TouchManagerView getTouchView() {
        if (touchView == null) {
            touchView = new TouchManagerView(getController());
        }
        return touchView;
    }

    public void currentPageChanged(final String pageText, final String bookTitle) {
        if (LengthUtils.isEmpty(pageText)) {

            return;
        }

        final AppSettings app = AppSettings.current();
        if (IUIManager.instance.isTitleVisible(this) && app.pageInTitle) {
           // getWindow().setTitle("(" + pageText + ") " + bookTitle);
            pagetext.setText(pageText);

            getWindow().setTitle(bookTitle);
            return;
        }

        if (app.pageNumberToastPosition == ToastPosition.Invisible) {
            return;
        }
        if (pageNumberToast != null) {
            pageNumberToast.setText(pageText);
        } else {
            pageNumberToast = Toast.makeText(this, pageText, Toast.LENGTH_SHORT);
        }

        pageNumberToast.setGravity(app.pageNumberToastPosition.position, 0, 0);
        pageNumberToast.show();
    }

    public void zoomChanged(final float zoom) {
        if (getZoomControls().isShown()) {
            return;
        }

        final AppSettings app = AppSettings.current();

        if (app.zoomToastPosition == ToastPosition.Invisible) {
            return;
        }

        final String zoomText = String.format("%.2f", zoom) + "x";

        if (zoomToast != null) {
            zoomToast.setText(zoomText);
        } else {
            zoomToast = Toast.makeText(this, zoomText, Toast.LENGTH_SHORT);
        }

        zoomToast.setGravity(app.zoomToastPosition.position, 0, 0);
        zoomToast.show();
    }

    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        getController().beforePostCreate();
        super.onPostCreate(savedInstanceState);
        getController().afterPostCreate();
    }

    public PageViewZoomControls getZoomControls() {
        if (zoomControls == null) {
            zoomControls = new PageViewZoomControls(this, getController().getZoomModel());
            zoomControls.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
        }
        return zoomControls;
    }

    public SearchControls getSearchControls() {
        if (searchControls == null) {
            searchControls = new SearchControls(this);
        }
        return searchControls;
    }

    public ManualCropView getManualCropControls() {
        if (cropControls == null) {
            cropControls = new ManualCropView(getController());
        }
        return cropControls;
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        menu.clear();
        menu.setHeaderTitle(R.string.app_name);
        menu.setHeaderIcon(R.drawable.application_icon);
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu_context, menu);
        updateMenuItems(menu);
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onCreateOptionsMenu(Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.clear();

        final MenuInflater inflater = getMenuInflater();

        if (hasNormalMenu()) {
            inflater.inflate(R.menu.mainmenu, menu);
        } else {
            inflater.inflate(R.menu.mainmenu_context, menu);
        }

        return true;
    }

    protected boolean hasNormalMenu() {
        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onMenuOpened(int, Menu)
     */
    @Override
    public boolean onMenuOpened(final int featureId, final Menu menu) {
        view.changeLayoutLock(true);
        IUIManager.instance.onMenuOpened(this);
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    protected void updateMenuItems(final Menu menu) {
        final AppSettings as = AppSettings.current();

        setMenuItemChecked(menu, as.fullScreen, R.id.mainmenu_fullscreen);
        setMenuItemVisible(menu, false, R.id.mainmenu_showtitle);
        setMenuItemChecked(menu, getZoomControls().getVisibility() == View.VISIBLE, R.id.mainmenu_zoom);

        final BookSettings bs = getController().getBookSettings();
        if (bs == null) {
            return;
        }

        setMenuItemChecked(menu, bs.nightMode, R.id.mainmenu_nightmode);
        setMenuItemChecked(menu, bs.autoLevels, R.id.mainmenu_autolevels);
        setMenuItemChecked(menu, bs.cropPages, R.id.mainmenu_croppages);
        setMenuItemChecked(menu, bs.splitPages, R.id.mainmenu_splitpages, R.drawable.viewer_menu_split_pages,
                R.drawable.viewer_menu_split_pages_off);

        final MenuItem navMenu = menu.findItem(R.id.mainmenu_nav_menu);
        if (navMenu != null) {
            final SubMenu subMenu = navMenu.getSubMenu();
            subMenu.removeGroup(R.id.actions_goToBookmarkGroup);
            if (AppSettings.current().showBookmarksInMenu && LengthUtils.isNotEmpty(bs.bookmarks)) {
                for (final Bookmark b : bs.bookmarks) {
                    addBookmarkMenuItem(subMenu, b);
                }
            }
        }

    }

    protected void addBookmarkMenuItem(final Menu menu, final Bookmark b) {
        final MenuItem bmi = menu.add(R.id.actions_goToBookmarkGroup, R.id.actions_goToBookmark, Menu.NONE, b.name);
        bmi.setIcon(R.drawable.viewer_menu_bookmark);
        setMenuItemExtra(bmi, "bookmark", b);
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onPanelClosed(int, Menu)
     */
    @Override
    public void onPanelClosed(final int featureId, final Menu menu) {
        menuClosedCalled = false;
        super.onPanelClosed(featureId, menu);
        if (!menuClosedCalled) {
            onOptionsMenuClosed(menu);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onOptionsMenuClosed(Menu)
     */
    @Override
    public void onOptionsMenuClosed(final Menu menu) {
        menuClosedCalled = true;
        IUIManager.instance.onMenuClosed(this);
        view.changeLayoutLock(false);
    }

    @Override
    public final boolean dispatchKeyEvent(final KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
            if (!hasNormalMenu()) {
                getController().getOrCreateAction(R.id.actions_openOptionsMenu).run();
                return true;
            }
        }

        if (getController().dispatchKeyEvent(event)) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    public void showToastText(final int duration, final int resId, final Object... args) {
        Toast.makeText(getApplicationContext(), getResources().getString(resId, args), duration).show();
    }
    //@Override
  /*  public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }*/
}
