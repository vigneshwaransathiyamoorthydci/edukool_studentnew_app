package org.ebookdroid.ui.settings;

import com.dci.edukool.student.R;
import org.ebookdroid.common.settings.AppSettings;
import org.ebookdroid.common.settings.SettingsManager;
import org.ebookdroid.common.settings.books.BookSettings;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.emdev.common.filesystem.PathFromUri;

import java.lang.reflect.Method;

public class BookSettingsActivity extends BaseSettingsActivity {

    private BookSettings current;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Uri uri = getIntent().getData();
        final String fileName = PathFromUri.retrieve(getContentResolver(), uri);
        current = SettingsManager.getBookSettings(fileName);
        if (current == null) {
            finish();
            return;
        }

        setRequestedOrientation(current.getOrientation(AppSettings.current()));

        SettingsManager.onBookSettingsActivityCreated(current);

        try {
            addPreferencesFromResource(R.xml.fragment_book);
        } catch (final ClassCastException e) {
            LCTX.e("Book preferences are corrupt! Resetting to default values.");

            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            final SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();

            PreferenceManager.setDefaultValues(this, R.xml.fragment_book, true);
            addPreferencesFromResource(R.xml.fragment_book);
        }

        decorator.decoratePreference(getRoot());
        decorator.decorateBooksSettings(current);
    }

    @Override
    protected void onPause() {
        SettingsManager.onBookSettingsActivityClosed(current);
        super.onPause();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
