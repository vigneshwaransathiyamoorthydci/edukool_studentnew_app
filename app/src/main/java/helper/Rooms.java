package helper;

/**
 * Created by abimathi on 05-Jun-17.
 */
public class Rooms {

    int id;
    String roomsname;
    String ssid;
    String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomsname() {
        return roomsname;
    }

    public void setRoomsname(String roomsname) {
        this.roomsname = roomsname;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
