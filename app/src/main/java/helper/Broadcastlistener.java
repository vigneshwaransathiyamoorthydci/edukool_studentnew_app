package helper;

/**
 * Created by iyyapparajr on 4/12/2017.
 */

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class Broadcastlistener
{
    private final int STUDENT_BROADCAST_RECEIVING_PORT = 2700;
    private final int TEACHER_BROADCAST_RECEIVING_PORT = 6464;
    private BroadcastMessageListener broadcastMessageListener;
    private boolean isRunning = true;
    private ReceiverThread receiver;
    private DatagramSocket receiverSocket;
    private DatagramSocket senderSocket;

    public void addBroadcastMessageListener(BroadcastMessageListener paramBroadcastMessageListener)
    {
        this.broadcastMessageListener = paramBroadcastMessageListener;
    }

    public void broadcastMessage(final String paramString1, final String paramString2)
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                Object localObject = paramString1;
                try
                {
                    if (Broadcastlistener.this.isRunning)
                    {
                        localObject = new DatagramPacket(((String)localObject).getBytes(), ((String)localObject).length(), InetAddress.getByName(paramString2), 6464);
                        Log.d("OneMasterT", "BroadCastMessage:" + paramString1);
                        if (!Broadcastlistener.this.senderSocket.isClosed()) {
                            Broadcastlistener.this.senderSocket.send((DatagramPacket)localObject);
                        }
                    }
                    return;
                }
                catch (SocketException localSocketException)
                {
                    localSocketException.printStackTrace();
                    return;
                }
                catch (IOException localIOException)
                {
                    localIOException.printStackTrace();
                }
            }
        }).start();
    }


   /* public void broadcastMessage(final String disconnect)
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                Object localObject = paramString;
                try
                {
                    if (Broadcastlistener.this.isRunning)
                    {
                        localObject = new DatagramPacket(((String)localObject).getBytes(), ((String)localObject).length(), InetAddress.getByName(paramString2), 2700);
                        Log.d("OneMasterT", "BroadCastMessage:" + paramString1);
                        if (!Broadcastlistener.this.senderSocket.isClosed()) {
                            Broadcastlistener.this.senderSocket.send((DatagramPacket)localObject);
                        }
                    }
                    return;
                }
                catch (SocketException localSocketException)
                {
                    localSocketException.printStackTrace();
                    return;
                }
                catch (IOException localIOException)
                {
                    localIOException.printStackTrace();
                }
            }
        }).start();
    }*/


    public void initialize()
    {
        try
        {
            this.senderSocket = new DatagramSocket(null);
            this.senderSocket.setReuseAddress(true);
            this.senderSocket.bind(null);
            this.senderSocket.setBroadcast(true);
            this.receiverSocket = new DatagramSocket(null);
            this.receiverSocket.setReuseAddress(true);
            this.receiverSocket.bind(new InetSocketAddress(2700));
            this.receiverSocket.setBroadcast(true);
            //return;
        }
        catch (SocketException localSocketException)
        {
            localSocketException.printStackTrace();
        }
    }


    public void initialize1()
    {
        try
        {
            this.senderSocket = new DatagramSocket(null);
            this.senderSocket.setReuseAddress(true);
            this.senderSocket.bind(null);
            this.senderSocket.setBroadcast(true);
            this.receiverSocket = new DatagramSocket(null);
            this.receiverSocket.setReuseAddress(true);
            this.receiverSocket.bind(new InetSocketAddress(2700));
            this.receiverSocket.setBroadcast(true);
            //return;
        }
        catch (SocketException localSocketException)
        {
            localSocketException.printStackTrace();
        }
    }


    public void receive()
    {
        this.receiver = new ReceiverThread();
        this.receiver.start();
    }

    public void releaseSockets()
    {
        this.isRunning = false;
        try
        {
            this.senderSocket.close();
            this.receiverSocket.close();
            if (this.receiver != null)
            {
                ReceiverThread localReceiverThread = this.receiver;
                this.receiver = null;
                localReceiverThread.interrupt();
            }
            return;
        }
        catch (Exception localException)
        {
            for (;;)
            {
                localException.printStackTrace();
            }
        }
    }

    public class ReceiverThread
            extends Thread
    {
        public ReceiverThread() {}

        public void run()
        {
            try
            {
                byte[] arrayOfByte = new byte[1024];
                for (;;)
                {
                    if (!Broadcastlistener.this.isRunning) {
                        return;
                    }
                    Object localObject = new DatagramPacket(arrayOfByte, arrayOfByte.length);
                    try
                    {
                        if (!Broadcastlistener.this.receiverSocket.isClosed())
                        {
                            Broadcastlistener.this.receiverSocket.receive((DatagramPacket)localObject);
                            localObject = new String(arrayOfByte, 0, ((DatagramPacket)localObject).getLength());
                           // Log.d("BroadcastUtils class :  : " + (String) localObject, "");
                            /*if (((String)localObject).startsWith("invS")) {*/
                                Broadcastlistener.this.broadcastMessageListener.onMessageReceived((String)localObject);
                           // }
                        }
                    }
                    catch (Exception localException2)
                    {
                        localException2.printStackTrace();
                    }
                }
                //return;
            }
            catch (Exception localException1)
            {
                localException1.printStackTrace();
            }
        }
    }





}

