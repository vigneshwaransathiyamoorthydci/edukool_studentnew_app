package helper;

/**
 * Created by abimathi on 21-Apr-17.
 */
public class Studentinfo {
    String studentname;
    String studentip;
    String studentrollnumber;

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getStudentip() {
        return studentip;
    }

    public void setStudentip(String studentip) {
        this.studentip = studentip;
    }

    public String getStudentrollnumber() {
        return studentrollnumber;
    }

    public void setStudentrollnumber(String studentrollnumber) {
        this.studentrollnumber = studentrollnumber;
    }
}
