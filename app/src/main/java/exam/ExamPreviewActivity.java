package exam;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * Created by pratheeba on 4/26/2017.
 */
public class ExamPreviewActivity extends Activity {
    SelfTestQuestionAdapter selfevalutation_adapter;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    static JSONArray quesList;
    String ExamIsVal;
    TextView exanname,dateattended,totalscore;
    String totalscoreupdated, dateatten;
    int ExamID;
    String Question;
    String OptionA;
    String OptionB;
    String OptionC;
    String OptionD;
    int totalScore;
    String  CorrectAnswer;
    int ObtainedScore;
    String studentname,profilename;
    String StudentID;
    String RollNo;
    ImageView close;
    ListView listView,self_asses_ques_ans_list;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.exam_preview_layout);

        Bundle bundle = getIntent().getExtras();

        ExamID = bundle.getInt("ExamID");

        self_asses_ques_ans_list = (ListView) findViewById(R.id.listView);
        exanname= (TextView) findViewById(R.id.exanname);
        dateattended= (TextView) findViewById(R.id.datearrended);
        totalscore= (TextView) findViewById(R.id.mark);

        close= (ImageView) findViewById(R.id.close);
        try {
            totalScore=0;
            loadQuestions();
        }
        catch (Exception e){
            e.printStackTrace();
        }


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void loadQuestions() throws Exception {
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
//Get the text file
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {

                DatabaseHandler db = new DatabaseHandler(ExamPreviewActivity.this);
            List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(ExamID);

            for (ExamDetails cn : examdetailsList) {
                String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID() + " ,BatchID: " + cn.getBatchID() + " ,IsResultPublished: " + cn.getIsResultPublished() + " ,ExamShelfID: " + cn.getExamShelfID() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                // Writing Contacts to log
                Log.d("Exam2: ", log);
                totalscoreupdated = cn.getTotalScore()+"";
                dateatten = cn.getDateAttended()+"";
            }
              String descriptionval  = db.getAllExamsDetailsusingExamId(ExamID);
            exanname.setText(descriptionval);
            dateattended.setText("Date Attended "+dateatten);
            SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());

            Cursor tc=obj.retrive("tblStudent");
            while(tc.moveToNext())
            {
                studentname = tc.getString(tc.getColumnIndex("FirstName"));
                StudentID   = tc.getString(tc.getColumnIndex("StudentID"));
                RollNo      = tc.getString(tc.getColumnIndex("RollNo"));
                profilename = tc.getString(tc.getColumnIndex("PhotoFilename"));
            }
               /* for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);

                    Question =cn.getQuestion();
                    OptionA =cn.getOptionA();
                    OptionB =cn.getOptionB();
                    OptionC =cn.getOptionC();
                    OptionD =cn.getOptionD();
                    CorrectAnswer = cn.getCorrectAnswer();
                    ObtainedScore= cn.getObtainedScore();*/

            descriptionval  = db.getAllExamsDetailsusingExamId(ExamID,"TeacherConducted");
          //  examname.setText(descriptionval);

                List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsuseingID(ExamID);

           // List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(ExamID);
            for (QuestionDetails cn : questiondetailsList) {
                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                // Writing Contacts to log
                Log.d("Question3: ", log);
                Question =cn.getQuestion();
                OptionA =cn.getOptionA();
                OptionB =cn.getOptionB();
                OptionC =cn.getOptionC();
                OptionD =cn.getOptionD();
                CorrectAnswer = cn.getCorrectAnswer();
                ObtainedScore= cn.getObtainedScore();
                totalScore = cn.getMark()+totalScore;


              //  obtainedscoreval.setText("Marks Obtained: "+ObtainedScore);



                // obtainedscoreval.setText("Marks Obtained: " + ObtainedScore);

                    plan_list_array.add(new QuestionPojo(Question, OptionA, OptionB, OptionC, OptionD, String.valueOf(CorrectAnswer), cn.getStudentAnswer(), studentname, RollNo, StudentID, questiondetailsList.size()+"", "StudentPhotPath",0));

                }

                try {
                    totalscore.setText("Mark: " +ObtainedScore+"/"+totalScore);
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            selfevalutation_adapter = new SelfTestQuestionAdapter(ExamPreviewActivity.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);


        }
        catch (Exception e) {

            e.printStackTrace();
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }

    @Override
    public void onBackPressed() {

    }


    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onResume();
    }

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

}
