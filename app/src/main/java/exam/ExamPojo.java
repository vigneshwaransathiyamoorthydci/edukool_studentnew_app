package exam;

/**
 * Created by pratheeba on 4/28/2017.
 */
public class ExamPojo {

    public boolean isClick() {
        return click;
    }

    public void setClick(boolean click) {
        this.click = click;
    }

    boolean click;

    String exam_name;

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public void setExam_id(int exam_id) {
        this.exam_id = exam_id;
    }


    public int getExam_id() {
        return exam_id;
    }

    int exam_id;
    public String getTopicDescription() {
        return TopicDescription;
    }

    public void setTopicDescription(String topicDescription) {
        TopicDescription = topicDescription;
    }

    String TopicDescription;

    public ExamPojo(String exam_name, int exam_id,String TopicDescription) {
        this.exam_name=exam_name;
        this.exam_id=exam_id;
        this.TopicDescription = TopicDescription;
    }
}
