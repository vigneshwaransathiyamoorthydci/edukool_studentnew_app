package exam;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import com.dci.edukool.student.R;

/**
 * Created by pratheeba on 4/25/2017.
 */
public class ExamListAdapter extends ArrayAdapter<ExamPojo> implements View.OnClickListener{

    private ArrayList<ExamPojo> dataSet;
    Context mContext;
    RadioButton selected=null;
    // View lookup cache
    private static class ViewHolder {
        RadioButton examradio;
        RelativeLayout parlay;
    }

    public ExamListAdapter(ArrayList<ExamPojo> data, Context context) {
        super(context, R.layout.exam_adapter, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        ExamPojo dataModel=(ExamPojo)object;

       /* switch (v.getId())
        {
          *//*  case R.id.item_info:
                Snackbar.make(v, "Release date " + dataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;*//*
        }*/
    }

    private int lastPosition = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final ExamPojo dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
       final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.exam_adapter, parent, false);
            viewHolder.examradio = (RadioButton) convertView.findViewById(R.id.examradio);
            viewHolder.parlay = (RelativeLayout) convertView.findViewById(R.id.parlay);

           /* if (position % 2 == 1) {
                convertView.setBackgroundResource(R.drawable.altest);
            }
            else  {
                convertView.setBackgroundResource(R.drawable.othertest);
            }*/
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        // result.startAnimation(animation);
        lastPosition = position;
        if (dataSet.get(position).isClick()){
            viewHolder.examradio.setChecked(true);

        }
        else{
            viewHolder.examradio.setChecked(false);

        }
        viewHolder.examradio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               // if(selected != null)
                //{

                    //selected.setChecked(false);
                //}

                for(int i=0; i<dataSet.size();i++){
                    if(i==position){
                        dataSet.get(position).setClick(true);

                    }
                    else{
                        dataSet.get(i).setClick(false);

                    }
                }
                StudentExamListActivity stu = new StudentExamListActivity();
                try {
                    Intent in=new Intent("Exam");
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("ExamIDValue", dataModel.getExam_id());
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);
                  //  stu.loadQuestions();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                viewHolder.examradio.setChecked(true);
                notifyDataSetChanged();
                //selected = viewHolder.examradio;
            }
        });
        viewHolder.examradio.setText(dataModel.getTopicDescription());
        //Utils.setTextviewtypeface(2, viewHolder.examradio);
        // Return the completed view to render on screen
        return convertView;
    }
}
