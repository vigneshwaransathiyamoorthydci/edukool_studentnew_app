package exam;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.dci.edukool.student.R;

/**
 * Created by pratheeba on 4/25/2017.
 */
public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener{

    private ArrayList<DataModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        RelativeLayout parlay;
    }

    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

        Toast.makeText(mContext, "onclick", Toast.LENGTH_LONG).show();

       /* switch (v.getId())
        {
          *//*  case R.id.item_info:
                Snackbar.make(v, "Release date " + dataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;*//*
        }*/
    }





    private int lastPosition = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
      final DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
       final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.parlay = (RelativeLayout) convertView.findViewById(R.id.parlay);


            convertView.setBackgroundResource(R.drawable.othertest);

                    result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
              if(dataModel.isClick()){
                  viewHolder.parlay.setBackgroundResource(R.drawable.altest);
                  viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.pink));


              }
           else{
                  viewHolder.parlay.setBackgroundResource(R.drawable.othertest);
                  viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.white));

              }



        viewHolder.parlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ExamId = dataModel.getExamId();

                dataSet.get(position).setClick(true);
                for(int d=0; d<dataSet.size(); d++)
                {
                    if(d!=position)
                    {
                        dataSet.get(d).setClick(false);
                    }
                }
                notifyDataSetChanged();

                try {
                    Intent in = new Intent("SelfEvaluation");
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("SelfExamId", ExamId);
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);
                    //  stu.loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
       // result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(dataModel.getName());
        // Return the completed view to render on screen
        return convertView;
    }
}
