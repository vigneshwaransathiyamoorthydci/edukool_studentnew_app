package exam;
/**
 * Created by pratheeba on 4/21/2017.
 */
import android.app.Dialog;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.app.Activity;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.dci.edukool.student.R;

public class ExamCompletedActivity extends Activity {
    private LinearLayout mainLinearLayout;
    private RelativeLayout linearLayout1;
    private RelativeLayout linearLayout;
    private RelativeLayout linearLayout2;
    private TextView quizQuestion,timer_text;
    private RadioGroup radioGroup;
    private RadioButton optionOne;
    private RadioButton optionTwo;
    private RadioButton optionThree;
    private RadioButton optionFour;
    private int currentQuizQuestion;
    private int quizCount;
    static JSONArray quesList;
    static JSONArray examDetailList;
    private int mQuestionIndex = 0;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    SelfTestQuestionAdapter selfevalutation_adapter;
    ExamGridAdapter exam_grid_adapter;
    private Dialog dialog;
    private TextView headerText;
    private TextView bodyText;
    private Button closeButton, yesBtn;
    private Button results;
    private Button layoutinputButton;
    ListView listView,self_asses_ques_ans_list;
    String selecdata;
    GridView exam_list_grid;
    ArrayList<DataModel> dataModels;
    private static CustomAdapter adapter;
    View header;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.self_asses_ques_ans_list_total);
        dataModels= new ArrayList<>();
        //dataModels=new ArrayList<String>();
        dataModels.add(new DataModel("Test 1"));
        dataModels.add(new DataModel("Test 2"));
        dataModels.add(new DataModel("Test 3"));
        self_asses_ques_ans_list = (ListView) findViewById(R.id.listView);
        try {
            // loadExamList();
            loadQuestions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void loadQuestions() throws Exception {
        String fileContent = new String();
        DatabaseHandler db = new DatabaseHandler(ExamCompletedActivity.this);
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();
//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();
            br.close();
            System.out.println("fileContent.length()" + fileContent.length());
            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                JSONObject resultObject = null;
                JSONArray jsonArray = null;
                QuizWrapper newItemObject = null;
                try {
                    resultObject = new JSONObject(fileContent);
                    System.out.println("Testing the water " + resultObject.toString());
                    jsonArray = resultObject.optJSONArray("questions");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonChildNode = null;
                    try {
                        jsonChildNode = jsonArray.getJSONObject(i);
                        int id = 1;
                        String question = jsonChildNode.getString("question");
                        JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                        int correctAnswer = jsonChildNode.getInt("correctIndex");
                        db.addExamFromTeacher(new StudentTeacherExam(1, question, answerOptions + "", correctAnswer + ""));
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                List<StudentTeacherExam> contacts = db.getAllExamsWritten();
                List<StudentTeacherExam> getexamatndjusnow = db.getAttendedExamJustNow();
                for (StudentTeacherExam cn : contacts) {
                    String log = "Exam_id: " + cn.getExam_id() + " ,Student_roll_number: " + cn.getStudent_roll_number() + " ,Selected_answer: " + cn.getSelected_answer();
                    // Writing Contacts to log
                    Log.d("Exam_id: ", log);
                    ArrayUtils.selectedAnswersFroCurrentExam.add(cn.getSelected_answer());
                }
                for (StudentTeacherExam cn : getexamatndjusnow) {
                    String log = "cn.getQuestion(): " + cn.getQuestion() + " ,options: " + cn.getOption_answer() + " ,correct_ans: " + cn.getCorrect_answer();
                    // Writing Contacts to log
                    Log.d("Exam_id: ", log);
                    JSONArray json = new JSONArray(cn.getOption_answer());

                    for(int i = 0; i <  ArrayUtils.selectedAnswersFroCurrentExam.size(); i++){
                        selecdata = ArrayUtils.selectedAnswersFroCurrentExam.get(i);
                        System.out.println(selecdata+"selecdata");
                    }
                    try {
                        plan_list_array.add(new QuestionPojo(cn.getQuestion(), "", "", "", "", cn.getCorrect_answer(),selecdata, "StudentName", "StudentRollNumber", "StudentID", "TotalNoQuestion", "StudentPhotPath",0));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                selfevalutation_adapter = new SelfTestQuestionAdapter(ExamCompletedActivity.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);

            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }
    }
    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;*/
      //  getWindow().setAttributes(lp);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}



