package exam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pratheeba on 4/24/2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "studentsExamManager";

    // examswritten table name
    private static final String TABLE_EXAM_WRITTEN = "examswritten";
    //Exam from teacher
    private static final String TABLE_EXAM_FROM_TEACHER = "examfromteacher";


    //ExamDetails
    private static final String TABLE_EXAM_DETAILS = "ExamDetails";
    private static final String EXAM_ID = "ExamID";
    private static final String EXAM__CATEGORY_ID = "ExamCategoryID";
    private static final String EXAM_CATEGORY_NAME = "ExamCategoryName";
    private static final String EXAM_CODE = "ExamCode";
    private static final String EXAM_DESCRIPTION = "ExamDescription";
    private static final String EXAM_SEQUENCE = "ExamSequence";
    private static final String EXAM_DATE = "ExamDate";
    private static final String EXAM_TYPE_ID = "ExamTypeID";
    private static final String EXAM_TYPE = "ExamType";
    private static final String SUBJECT_ID = "SubjectID";
    private static final String SUBJECT = "Subject";
    private static final String EXAM_DURATION = "ExamDuration";
    private static final String SCHOOL_ID = "SchoolID";
    private static final String CLASS_ID = "ClassID";
    private static final String BATCH_ID = "BatchID";
    private static final String IS_RESULT_PUBLISHED = "IsResultPublished";
    private static final String EXAM_SHELF_ID = "ExamShelfID";
    private static final String TIME_TAKEN = "TimeTaken";
    private static final String DATE_ATTENDED = "DateAttended";
    private static final String TOTAL_SCORE = "TotalScore";

    //tblExamQuestions
    private static final String TABLE_EXAM_QUESTION= "ExamQuestions";
    private static final String QUESTION_ID = "QuestionID";
    private static final String QUESTION_EXAM_ID = "ExamID";
    private static final String TOPIC_ID = "TopicID";
    private static final String TOPIC_NAME = "TopicName";
    private static final String ASPECT_ID = "AspectID";
    private static final String ASPECT = "Aspect";
    private static final String QUESTION = "Question";
    private static final String QUESTION_NUMBER = "QuestionNumber";
    private static final String OPTION_A = "OptionA";
    private static final String OPTION_B = "OptionB";
    private static final String OPTION_C = "OptionC";
    private static final String OPTION_D = "OptionD";
    private static final String QUESTION_CORRECT_ANSWER = "CorrectAnswer";
    private static final String MARK = "Mark";
    private static final String NEGATIVE_MARK = "Negative_Mark";
    private static final String STUDENT_ANSWER = "StudentAnswer";
    private static final String IS_CORRECT = "IsCorrect";
    private static final String OBTAINED_SCORE = "ObtainedScore";
    private static final String CREATED_ON = "CreatedOn";
    private static final String RESULT_FROM_TEACHER = "TeacherResult";
    private static final String MODIFIED_ON = "ModifiedOn";

    //tblAspectReport
    private static final String TABLE_ASPECT_REPORT= "AspectReport";
    private static final String AS_REPORT_ID = "ReportID";
    private static final String AS_EXAM_ID = "ExamID";
    private static final String AS_TOPIC_ID = "TopicID";
    private static final String AS_TOPIC_NAME = "TopicName";
    private static final String AS_ASPECT_ID = "AspectID";
    private static final String AS_ASPECT = "Aspect";
    private static final String ASPECT_QUESTION_ID = "QuestionID";
    private static final String NO_OF_CORRECT_ANSWER = "NoOfCorrectAnswers";
    private static final String NO_OF_WRONG_ANSWER = "NoOfWrongAnswers";






    // examswritten Table Columns names
    private static final String KEY_ID = "id";
    private static final String STUDENT_ROLL_NUMBER = "student_roll_number";
    private static final String SELECTED_ANSWER = "selected_answer";



    //Exam from teacher

    private static final String KEY_EXAM_ID = "id";
    private static final String EXAM_QUESTIONS = "question";
    private static final String EXAM_ANSWER_OPTION = "option_answer";
    private static final String CORRECT_ANSWER = "correct_answer";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_EXAM_WRITTEN_TABLE = "CREATE TABLE " + TABLE_EXAM_WRITTEN + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + STUDENT_ROLL_NUMBER + " TEXT,"
                + SELECTED_ANSWER + " TEXT" + ")";
        db.execSQL(CREATE_EXAM_WRITTEN_TABLE);


        String CREATE_TABLE_EXAM_FROM_TEACHER = "CREATE TABLE " + TABLE_EXAM_FROM_TEACHER + "("
                + KEY_EXAM_ID + " INTEGER PRIMARY KEY," + EXAM_QUESTIONS + " TEXT,"+ EXAM_ANSWER_OPTION + " TEXT,"
                + CORRECT_ANSWER + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_EXAM_FROM_TEACHER);



        String CREATE_TABLE_EXAM_DETAILS = "CREATE TABLE " + TABLE_EXAM_DETAILS + "("
                + EXAM_ID + " INTEGER PRIMARY KEY,"+ EXAM__CATEGORY_ID + " INTEGER," + EXAM_CATEGORY_NAME + " TEXT,"+ EXAM_CODE + " TEXT,"+EXAM_DESCRIPTION + " TEXT,"+EXAM_SEQUENCE + " TEXT,"+EXAM_DATE + " TEXT,"+EXAM_TYPE_ID + " INTEGER,"+EXAM_TYPE + " TEXT,"+SUBJECT_ID + " INTEGER,"+SUBJECT + " TEXT,"+EXAM_DURATION + " INTEGER,"+SCHOOL_ID + " INTEGER,"+CLASS_ID + " INTEGER,"+BATCH_ID + " INTEGER,"+ IS_RESULT_PUBLISHED + " INTEGER,"+EXAM_SHELF_ID + " INTEGER,"+TIME_TAKEN + " INTEGER,"+DATE_ATTENDED + " TEXT," + TOTAL_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_EXAM_DETAILS);



        String CREATE_TABLE_EXAM_QUESTION = "CREATE TABLE " + TABLE_EXAM_QUESTION + "("
                + QUESTION_ID + " INTEGER PRIMARY KEY,"+ QUESTION_EXAM_ID + " INTEGER," + TOPIC_ID + " INTEGER,"+ TOPIC_NAME + " TEXT,"+ASPECT_ID + " INTEGER,"+ASPECT + " TEXT,"+QUESTION + " TEXT,"+QUESTION_NUMBER + " INTEGER,"+OPTION_A + " TEXT,"+OPTION_B + " TEXT,"+OPTION_C + " TEXT,"+OPTION_D + " TEXT,"+CORRECT_ANSWER + " TEXT,"+MARK + " INTEGER,"+NEGATIVE_MARK + " INTEGER,"+ STUDENT_ANSWER + " TEXT,"+IS_CORRECT + " INTEGER,"+OBTAINED_SCORE + " INTEGER," +CREATED_ON + " TEXT," +RESULT_FROM_TEACHER+ " TEXT," + MODIFIED_ON + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_EXAM_QUESTION);



        String CREATE_TABLE_ASPECT_REPORT = "CREATE TABLE " + TABLE_ASPECT_REPORT + "("
                + AS_REPORT_ID + " INTEGER PRIMARY KEY,"+ AS_EXAM_ID + " INTEGER," + AS_TOPIC_ID + " INTEGER,"+ AS_TOPIC_NAME + " TEXT,"+AS_ASPECT_ID + " INTEGER,"+AS_ASPECT + " TEXT,"+ASPECT_QUESTION_ID + " INTEGER,"+QUESTION_NUMBER + " INTEGER,"+NO_OF_CORRECT_ANSWER + " INTEGER," + NO_OF_WRONG_ANSWER + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_ASPECT_REPORT);
    }




    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServerTeacher(String subjectID) {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        //  String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE_ID+" = '"+ "2"+"' and "+SUBJECT_ID+"= '"+subjectID+"'";
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +IS_RESULT_PUBLISHED+" = '"+ "1"+"' and "+SUBJECT_ID+"= '"+subjectID+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(4));
                examdetailscal.setIsResultPublished(Integer.parseInt(cursor.getString(15)));

                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_WRITTEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_FROM_TEACHER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_QUESTION);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new examswritten
    void addExam(StudentTeacherExam examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(STUDENT_ROLL_NUMBER, examlist.getStudent_roll_number()); // examswritten Name
        values.put(SELECTED_ANSWER, examlist.getSelected_answer()); // examswritten Phone

        // Inserting Row
        db.insert(TABLE_EXAM_WRITTEN, null, values);
        db.close(); // Closing database connection
    }



    // Adding new examswritten
    public void addExamDetails(ExamDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_ID, examlist.getExamID());
        values.put(EXAM__CATEGORY_ID, examlist.getExamCategoryID());
        values.put(EXAM_CATEGORY_NAME, examlist.getExamCategoryName());
        values.put(EXAM_CODE, examlist.getExamCode());
        values.put(EXAM_DESCRIPTION, examlist.getExamDescription());
        values.put(EXAM_SEQUENCE, examlist.getExamSequence());
        values.put(EXAM_DATE, examlist.getExamDate());
        values.put(EXAM_TYPE_ID, examlist.getExamTypeID());
        values.put(EXAM_TYPE, examlist.getExamType());
        values.put(SUBJECT_ID, examlist.getSubjectID());
        values.put(SUBJECT, examlist.getSubject());
        values.put(EXAM_DURATION, examlist.getExamDuration());
        values.put(SCHOOL_ID, examlist.getSchoolID());
        values.put(CLASS_ID, examlist.getClassID());
        values.put(BATCH_ID, examlist.getBatchID());
        values.put(IS_RESULT_PUBLISHED, examlist.getIsResultPublished());
        values.put(EXAM_SHELF_ID, examlist.getExamShelfID());
        values.put(TIME_TAKEN, examlist.getTimeTaken());
        values.put(DATE_ATTENDED, examlist.getDateAttended());
        values.put(TOTAL_SCORE, examlist.getTotalScore());



        // Inserting Row
        db.insert(TABLE_EXAM_DETAILS, null, values);
        db.close(); // Closing database connection
    }



    // Updating single examswritten
    public int downloadUpdateExamDetails(ExamDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_ID, examlist.getExamID());
        values.put(EXAM__CATEGORY_ID, examlist.getExamCategoryID());
        values.put(EXAM_CATEGORY_NAME, examlist.getExamCategoryName());
        values.put(EXAM_CODE, examlist.getExamCode());
        values.put(EXAM_DESCRIPTION, examlist.getExamDescription());
        values.put(EXAM_SEQUENCE, examlist.getExamSequence());
        values.put(EXAM_DATE, examlist.getExamDate());
        values.put(EXAM_TYPE_ID, examlist.getExamTypeID());
        values.put(EXAM_TYPE, examlist.getExamType());
        values.put(SUBJECT_ID, examlist.getSubjectID());
        values.put(SUBJECT, examlist.getSubject());
        values.put(EXAM_DURATION, examlist.getExamDuration());
        values.put(SCHOOL_ID, examlist.getSchoolID());
        values.put(CLASS_ID, examlist.getClassID());
        values.put(BATCH_ID, examlist.getBatchID());
        values.put(IS_RESULT_PUBLISHED, examlist.getIsResultPublished());
        values.put(EXAM_SHELF_ID, examlist.getExamShelfID());
        values.put(TIME_TAKEN, examlist.getTimeTaken());
        values.put(DATE_ATTENDED, examlist.getDateAttended());
        values.put(TOTAL_SCORE, examlist.getTotalScore());

        // updating row
        return db.update(TABLE_EXAM_DETAILS, values, EXAM_ID + " = ?",
                new String[] { String.valueOf(examlist.getExamID()) });
    }



    public  boolean CheckIsIDAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_EXAM_DETAILS + " where " + EXAM_ID + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    // Updating single examswritten
    public int updateExamDetails(ExamDetails examsdetails) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TIME_TAKEN, examsdetails.getTimeTaken());
        values.put(EXAM_TYPE, examsdetails.getExamType());
        values.put(DATE_ATTENDED, examsdetails.getDateAttended());
        values.put(TOTAL_SCORE, examsdetails.getTotalScore());

        // updating row
        return db.update(TABLE_EXAM_DETAILS, values, EXAM_ID + " = ?",
                new String[] { String.valueOf(examsdetails.getExamID()) });
    }




    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServer(String subjectID) {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE+" = '"+ "Self-Evaluation"+"' and "+SUBJECT_ID+"= '"+subjectID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(4));
                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }
    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServer() {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE+" = '"+ "Self-Evaluation"+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(4));
                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }


    // Getting All examswritten
    public String getAllExamsDetailsusingExamDate(int ExamID,String examtype) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where "+EXAM_TYPE+" = '"+ examtype+ "' and " +EXAM_ID+" = '"+ ExamID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
                //  examdetailsList.add(examdetails);
                return cursor.getString(18);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }
    // Getting All examswritten
    public String getAllExamsDetailsusingExamSubject(int ExamID,String examtype) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where "+EXAM_TYPE+" = '"+ examtype+ "' and " +EXAM_ID+" = '"+ ExamID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
                //  examdetailsList.add(examdetails);
                return cursor.getString(10);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }
    // Getting All examswritten
    public String getAllExamsDetailsusingExamId(int ExamID,String examtype) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where "+EXAM_TYPE+" = '"+ examtype+ "' and " +EXAM_ID+" = '"+ ExamID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
                //  examdetailsList.add(examdetails);
                return cursor.getString(4);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }


    // Updating single examswritten
    public int updatexamdetailsByID(ExamDetails examdetails) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IS_RESULT_PUBLISHED, examdetails.getIsResultPublished());

        // updating row
        return db.update(TABLE_EXAM_DETAILS, values, EXAM_ID + " = ?",
                new String[] { String.valueOf(examdetails.getExamID()) });
    }

    // Getting All examswritten
    public String getAllExamsDateusingExamId(int ExamID,String examtype) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where "+EXAM_TYPE+" = '"+ examtype+ "' and " +EXAM_ID+" = '"+ ExamID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
                //  examdetailsList.add(examdetails);
                return cursor.getString(18);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }
    // Adding new tblExamQuestions
    public void addExamQuestions(QuestionDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, examlist.getQuestionID()); // examswritten Name
        values.put(QUESTION_EXAM_ID, examlist.getExamID()); // examswritten Phone
        values.put(TOPIC_ID, examlist.getTopicID()); // examswritten Phone
        values.put(TOPIC_NAME, examlist.getTopicName()); // examswritten Phone
        values.put(ASPECT_ID, examlist.getAspectID()); // examswritten Phone
        values.put(ASPECT, examlist.getAspect()); // examswritten Phone
        values.put(QUESTION, examlist.getQuestion()); // examswritten Phone
        values.put(QUESTION_NUMBER, examlist.getQuestionNumber()); // examswritten Phone
        values.put(OPTION_A, examlist.getOptionA()); // examswritten Phone
        values.put(OPTION_B, examlist.getOptionB()); // examswritten Phone
        values.put(OPTION_C, examlist.getOptionC()); // examswritten Phone
        values.put(OPTION_D, examlist.getOptionD()); // examswritten Phone
        values.put(CORRECT_ANSWER, examlist.getCorrectAnswer()); // examswritten Phone
        values.put(MARK, examlist.getMark()); // examswritten Phone
        values.put(NEGATIVE_MARK, examlist.getNegative_Mark()); // examswritten Phone
        values.put(STUDENT_ANSWER, examlist.getStudentAnswer()); // examswritten Phone
        values.put(IS_CORRECT, examlist.getIsCorrect()); // examswritten Phone
        values.put(OBTAINED_SCORE, examlist.getObtainedScore()); // examswritten Phone
        values.put(CREATED_ON, examlist.getCreatedOn()); // examswritten Phone
        values.put(RESULT_FROM_TEACHER, examlist.getResultFromTeacher()); // examswritten Phone
        values.put(MODIFIED_ON, examlist.getModifiedOn()); // examswritten Phone


        // Inserting Row
        db.insert(TABLE_EXAM_QUESTION, null, values);
        db.close(); // Closing database connection
    }

    // Adding new examswritten
    void addExamFromTeacher(StudentTeacherExam examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_QUESTIONS, examlist.getQuestion()); // examswritten Name
        values.put(EXAM_ANSWER_OPTION, examlist.getOption_answer()); // examswritten Phone
        values.put(CORRECT_ANSWER, examlist.getCorrect_answer()); // examswritten Phone

        // Inserting Row
        db.insert(TABLE_EXAM_FROM_TEACHER, null, values);
        db.close(); // Closing database connection
    }

    // Getting single examswritten
    StudentTeacherExam getExamsWritten(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EXAM_WRITTEN, new String[] {
                        KEY_ID,
                        STUDENT_ROLL_NUMBER, SELECTED_ANSWER }, KEY_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        StudentTeacherExam examswritten = new StudentTeacherExam(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return examswritten;
    }

    // Getting All examswritten
    public List<StudentTeacherExam> getAllExamsWritten() {
        List<StudentTeacherExam> examResultList = new ArrayList<StudentTeacherExam>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_WRITTEN;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentTeacherExam examswritten = new StudentTeacherExam();
                examswritten.setExam_id(Integer.parseInt(cursor.getString(0)));
                examswritten.setStudent_roll_number(cursor.getString(1));
                examswritten.setSelected_answer(cursor.getString(2));
                // Adding examswritten to list
                examResultList.add(examswritten);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examResultList;
    }

    // Getting All examswritten
    public List<QuestionDetails> getAllExamcompleted() {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION +" where " +RESULT_FROM_TEACHER+" = '"+ "Completed"+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }
    // Getting All examswritten
    public List<QuestionDetails> getAllExamcompletedSelf() {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION +" where " +RESULT_FROM_TEACHER+" = '"+ "Self-EvaluationCompleted"+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }



    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestionsuseingID(int ExamID) {
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION + " where " + QUESTION_EXAM_ID+" = '"+ ExamID+"'";
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setResultFromTeacher(cursor.getString(19));
                questionsdetails.setModifiedOn(cursor.getString(20));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }

    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestions() {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setModifiedOn(cursor.getString(19));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }

    // Getting All examswritten
    public List<ExamDetails> getAllExamUsingExamId(int ExamId) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS +" where " + EXAM_ID + " = '" + ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));
                // Adding examswritten to list
                examdetailsList.add(examdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetailsList;
    }




    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestionsUsingExamId(int ExamId) {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION +" where " +EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setModifiedOn(cursor.getString(19));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }

    // Getting All examswritten
    public String getAllExamsDetailsusingExamId(int ExamID) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_ID+" = '"+ ExamID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
              //  examdetailsList.add(examdetails);
           return cursor.getString(4);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }
    // Getting All examswritten
    public List<ExamDetails> getAllExamsDetails() {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));
                // Adding examswritten to list
                examdetailsList.add(examdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetailsList;
    }



    // Getting All examswritten
    public List<StudentTeacherExam> getAttendedExamJustNow() {
        List<StudentTeacherExam> examResultList = new ArrayList<StudentTeacherExam>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_FROM_TEACHER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentTeacherExam examswritten = new StudentTeacherExam();
                examswritten.setExam_id(Integer.parseInt(cursor.getString(0)));
                examswritten.setQuestion(cursor.getString(1));
                examswritten.setOption_answer(cursor.getString(2));
                examswritten.setCorrect_answer(cursor.getString(3));
                // Adding examswritten to list
                examResultList.add(examswritten);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examResultList;
    }


    // Updating single examswritten
    public int downloadUpdateQuestionDetailsByID(QuestionDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, examlist.getQuestionID()); // examswritten Name
        values.put(QUESTION_EXAM_ID, examlist.getExamID()); // examswritten Phone
        values.put(TOPIC_ID, examlist.getTopicID()); // examswritten Phone
        values.put(TOPIC_NAME, examlist.getTopicName()); // examswritten Phone
        values.put(ASPECT_ID, examlist.getAspectID()); // examswritten Phone
        values.put(ASPECT, examlist.getAspect()); // examswritten Phone
        values.put(QUESTION, examlist.getQuestion()); // examswritten Phone
        values.put(QUESTION_NUMBER, examlist.getQuestionNumber()); // examswritten Phone
        values.put(OPTION_A, examlist.getOptionA()); // examswritten Phone
        values.put(OPTION_B, examlist.getOptionB()); // examswritten Phone
        values.put(OPTION_C, examlist.getOptionC()); // examswritten Phone
        values.put(OPTION_D, examlist.getOptionD()); // examswritten Phone
        values.put(CORRECT_ANSWER, examlist.getCorrectAnswer()); // examswritten Phone
        values.put(MARK, examlist.getMark()); // examswritten Phone
        values.put(NEGATIVE_MARK, examlist.getNegative_Mark()); // examswritten Phone
        values.put(STUDENT_ANSWER, examlist.getStudentAnswer()); // examswritten Phone
        values.put(IS_CORRECT, examlist.getIsCorrect()); // examswritten Phone
        values.put(OBTAINED_SCORE, examlist.getObtainedScore()); // examswritten Phone
        values.put(CREATED_ON, examlist.getCreatedOn()); // examswritten Phone
        values.put(RESULT_FROM_TEACHER, examlist.getResultFromTeacher()); // examswritten Phone
        values.put(MODIFIED_ON, examlist.getModifiedOn()); // examswritten Phone

        // updating row
        return db.update(TABLE_EXAM_QUESTION, values, QUESTION_ID + " = ?",
                new String[] { String.valueOf(examlist.getExamID()) });
    }

    // Updating single examswritten
    public int updateQuestionDetailsByID(QuestionDetails questionDetails) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RESULT_FROM_TEACHER, questionDetails.getResultFromTeacher());

        // updating row
        return db.update(TABLE_EXAM_QUESTION, values, EXAM_ID + " = ?",
                new String[] { String.valueOf(questionDetails.getExamID()) });
    }


    // Updating single examswritten
    public int updateQuestionDetails(QuestionDetails questiondetails) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, questiondetails.getQuestionID());
        values.put(EXAM_ID, questiondetails.getExamID());
        values.put(QUESTION_NUMBER, questiondetails.getQuestionNumber());
        values.put(STUDENT_ANSWER, questiondetails.getStudentAnswer());
        values.put(IS_CORRECT, questiondetails.getIsCorrect());
        System.out.println("questiondetails.getStudentAnswer()" + questiondetails.getStudentAnswer());
        values.put(CREATED_ON, questiondetails.getCreatedOn());
        values.put(MODIFIED_ON, questiondetails.getModifiedOn());
        values.put(OBTAINED_SCORE, questiondetails.getObtainedScore());

        String[] args = new String[]{questiondetails.getQuestionNumber()+"", questiondetails.getExamID()+""};

       // return db.update(TABLE_EXAM_QUESTION, values, QUESTION_NUMBER + "=? AND" + EXAM_ID + "=?", args);
        // updating row
        return db.update(TABLE_EXAM_QUESTION, values, QUESTION_ID + " = ?",
                new String[] { String.valueOf(questiondetails.getQuestionID()) });

    }


    // Updating single examEvaluation
    public int updateQuestionDetailsResult(QuestionDetails questiondetails) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IS_CORRECT, questiondetails.getIsCorrect());
        values.put(OBTAINED_SCORE, questiondetails.getObtainedScore());
        System.out.println(questiondetails.getIsCorrect() + "questiondetails.getIsCorrect()");
        System.out.println(questiondetails.getObtainedScore()+"questiondetails.getObtainedScore()");

        // updating row
        return db.update(TABLE_EXAM_QUESTION, values, QUESTION_NUMBER + " = ?",
                new String[] { String.valueOf(questiondetails.getQuestionNumber()) });
    }

    // Updating single examswritten
    public int updateExamsWritten(StudentTeacherExam examswritten) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(STUDENT_ROLL_NUMBER, examswritten.getStudent_roll_number());
        values.put(SELECTED_ANSWER, examswritten.getSelected_answer());

        // updating row
        return db.update(TABLE_EXAM_WRITTEN, values, KEY_ID + " = ?",
                new String[] { String.valueOf(examswritten.getExam_id()) });
    }

    // Deleting single examswritten
    public void deleteExamsWritten(StudentTeacherExam examswritten) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXAM_WRITTEN, KEY_ID + " = ?",
                new String[] { String.valueOf(examswritten.getExam_id()) });
        db.close();
    }

 /*   public String getExamsSeletedAnswer() {

        String query = "SELECT * FROM " +TABLE_EXAM_WRITTEN+" WHERE "+ SELECTED_ANSWER;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }


        return ;
    }*/
    // Getting examswritten Count
    public int getExamsWrittenCount() {
        String countQuery = "SELECT  * FROM " + TABLE_EXAM_WRITTEN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

}
