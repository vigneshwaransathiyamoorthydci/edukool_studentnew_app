package exam;

/**
 * Created by Pratheeba on 6/5/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import Utils.Utils;
import java.util.List;
import com.dci.edukool.student.R;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class CustomSpinnerAdapter extends ArrayAdapter {


    Utils utils;
    private Context context;
    private List<String> itemList;
    Activity act;
    public CustomSpinnerAdapter(Context context, int textViewResourceId,List<String> itemList) {

        super(context, textViewResourceId,itemList);
        this.context=context;
        act= (Activity) context;
        this.itemList=itemList;
        utils=new Utils(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(itemList.get(position));
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        utils.setTextviewtypeface(2,make);

        make.setText(itemList.get(position));

        return row;
    }

}
