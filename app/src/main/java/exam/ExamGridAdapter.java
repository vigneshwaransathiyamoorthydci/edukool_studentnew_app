package exam;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;



/**
 * Created by pratheeba on 4/21/2017.
 */

;import java.util.ArrayList;

import com.dci.edukool.student.R;

import drawboard.MyDialog;

public class ExamGridAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;
    int layoutId;
    ProgressDialog mProgressDialog;
    public static final String MY_PREFS_NAME = "GENERALINFO";
    int[] images = new int[] {R.drawable.imageblue, R.drawable.imagemaroon,R.drawable.imagecyan, R.drawable.imagedrakblue,R.drawable.imagegreen, R.drawable.imageorange, R.drawable.imagepink,R.drawable.imagepurple, R.drawable.imageyell, R.drawable.imagered};
    private Button closeButton, yesBtn;
    private Button results;
    private Dialog dialog;
    Holder holder;
    static SharedPreferences preftutorial;
    String user_id_main;
    static ArrayList<QuestionPojo> planListAdapter = new ArrayList<QuestionPojo>();

    public ExamGridAdapter(Context context, int textViewResourceId,
                                   ArrayList<QuestionPojo> list) {
        this.context = context;
        ExamGridAdapter.planListAdapter = list;
        layoutId = textViewResourceId;
    }

    @Override
    public int getCount() {
        return planListAdapter.size();
    }

    @Override
    public QuestionPojo getItem(int position) {

        return planListAdapter.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        RelativeLayout layout;
        if (convertView == null) {
            // set layout view initialize the resource Id
            layout = (RelativeLayout) View.inflate(context, layoutId, null);
            holder = new Holder();

            holder.subjects_exam = (TextView) layout.findViewById(R.id.subjects_exam);

            holder.pargridlay= (RelativeLayout) layout.findViewById(R.id.pargridlay);
            int imageId = (int)(Math.random() * images.length);

// Set the image
            holder.pargridlay.setBackgroundResource(images[imageId]);

            layout.setTag(holder);
        } else {
            layout = (RelativeLayout) convertView;
            view = layout;
            holder = (Holder) layout.getTag();
        }


        holder.subjects_exam.setText(Html.fromHtml(planListAdapter.get(position).getExamdesc()));

        holder. pargridlay .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stratExamPopup(planListAdapter.get(position).getExamID());
            }
        });




        return layout;
    }



    public void stratExamPopup(final int examID) {
        dialog = new MyDialog(context);
       // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.start_exam_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button2);
        yesBtn = (Button) dialog.findViewById(R.id.button3);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();



            }
        });
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(context, QuizActivity.class);
                intent.putExtra("Examquestion", "SelfEvaluation");
                intent.putExtra("ExamID", examID);
                context.startActivity(intent);

                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public int getCurrentPosition() {
        return currPosition;
    }


    //Setting pojo for user comment
    private class Holder {
        public TextView subjects_exam;
        public RelativeLayout pargridlay;


    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 1;
    }


}

