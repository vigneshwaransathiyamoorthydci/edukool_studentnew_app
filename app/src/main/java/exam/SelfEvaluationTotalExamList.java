package exam;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;

/**
 * Created by pratheeba on 4/21/2017.
 */


import android.app.Activity;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

public class SelfEvaluationTotalExamList extends Activity {

    private LinearLayout mainLinearLayout;
    private RelativeLayout linearLayout1;
    private RelativeLayout linearLayout;
    private RelativeLayout linearLayout2;
    private TextView quizQuestion,timer_text;
    private RadioGroup radioGroup;
    private RadioButton optionOne;
    private RadioButton optionTwo;
    private RadioButton optionThree;
    private RadioButton optionFour;
    private int currentQuizQuestion;
    private int quizCount;
    static JSONArray quesList;
    static JSONArray examDetailList;
    int mark;
    private int mQuestionIndex = 0;
    int SelfExamId;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    SelfTestQuestionAdapter selfevalutation_adapter;
    ExamGridAdapter exam_grid_adapter;
    private Dialog dialog;
    private TextView headerText,obtainedscoreval,examname,examdate,subname;
    private TextView bodyText;
    private Button closeButton, yesBtn;
    private Button results;
    private ImageView piechart,back;
    private Button layoutinputButton;
    ListView listView,self_asses_ques_ans_list;
    TextView stdname;
    GridView exam_list_grid;
    ArrayList<DataModel> dataModels;
    private static CustomAdapter adapter;
    String studentname,profilename;
    View header;
    DatabaseHandler db;
    BroadcastReceiver questionreceiver;
    String Question;
    int QuestionNumber;

    String OptionA;
    String OptionB;
    String OptionC;
    String OptionD;
    int QuestionID;
    String  StudentAnswer;
    int IsCorrect;
    int MarkForAnswer;
    int  ObtainedScore;
    String description,descriptionval,subject,dateva;
    int exid;
    String  CorrectAnswer;
    int ExamResponseID;
    int ExamIDVal;
    int studentID;
    String StudName;
    int TimeTaken;
    String DateAttended;
    int  TotalScore;
    int  StudentID;
    int  RollNo;
    ArrayList<String> desclist = new ArrayList<String>();
    ArrayList<Integer> idlist = new ArrayList<Integer>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.self_total_results);

        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);

        results = (Button) findViewById(R.id.results);
        piechart = (ImageView) findViewById(R.id.piechart);
        stdname= (TextView)findViewById(R.id.studentname);
        back= (ImageView) findViewById(R.id.back);
        linearLayout1 = (RelativeLayout) View.inflate(this,
                R.layout.student_self_asses_list, null);
        db=new DatabaseHandler(this);

        linearLayout = (RelativeLayout) View.inflate(this,
                R.layout.exams_grid_layout, null);
        linearLayout2 = (RelativeLayout) View.inflate(this,
                R.layout.self_asses_ques_ans_list_total, null);
        dataModels= new ArrayList<>();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        int RecentExamID= bundle.getInt("RecentExamID");
        String RecentExamIDValue = bundle.getString("RecentExamIDValue");


        //dataModels=new ArrayList<String>();

     /*   List<ExamDetails> examdetailsList = db.getAllExamsDetails();

        for (ExamDetails cn : examdetailsList) {
            String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " +
                    cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
            // Writing Contacts to log
            dataModels.add(new DataModel(cn.getExamDescription(),cn.getExamID()));

            Log.d("Exam2: ", log);
        }*/

        List<QuestionDetails> questiondetailsListusingID = db.getAllExamcompletedSelf();
        for(QuestionDetails cn : questiondetailsListusingID)
        {
            String log = " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() ;           // Writing Contacts to log
            if(idlist.contains(cn.getExamID())){

            }
            else{
                idlist.add(cn.getExamID());
            }

            //examPojo.add(new ExamPojo(cn.getTopicName(),cn.getExamID(),description));
            Log.d("Questionvdsf2: ", log);
        }

        for(int i = 0; i < idlist.size(); i++){

            description  = db.getAllExamsDetailsusingExamId(idlist.get(i),"Self-Evaluation");

            dataModels.add(new DataModel(description,idlist.get(i)));

        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listView = (ListView) linearLayout1.findViewById(R.id.listView);
        self_asses_ques_ans_list = (ListView) linearLayout2.findViewById(R.id.listView);

        obtainedscoreval = (TextView) linearLayout2.findViewById(R.id.markob);
        examname= (TextView) linearLayout2.findViewById(R.id.exacna);
        examdate = (TextView) linearLayout2.findViewById(R.id.examdatetext);
        subname= (TextView) linearLayout2.findViewById(R.id.subje);

        exam_list_grid= (GridView) linearLayout.findViewById(R.id.gridView);




        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());
        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            studentname= tc.getString(tc.getColumnIndex("FirstName"));
            profilename= tc.getString(tc.getColumnIndex("PhotoFilename"));

        }

        stdname.setText(studentname.toUpperCase());
        if(profilename!=null)setbackground(piechart,profilename);



      /*  listView.animate().setDuration(1000).alpha(0).
                withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        listView.setAlpha(1);
                    }
                });*/

        mainLinearLayout.addView(linearLayout1);

/*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    // loadExamList();
                    loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });*/

        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SelfEvaluationTotalExamList.this, SelfEvaluationResults.class);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        piechart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  try {
                    Intent intent = new Intent(SelfEvaluationTotalExamList.this, StudentExamPieChart.class);
                    startActivity(intent);


                }
                catch (Exception e){
                    e.printStackTrace();
                }*/
            }
        });
        exam_list_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try{
                    stratExamPopup();

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        if(RecentExamIDValue.equalsIgnoreCase("RecentExamIDValue")){
            try {
                loadQuestionst(RecentExamID);

                for(int i=0; i<dataModels.size(); i++)
                {
                    if(RecentExamID==dataModels.get(i).examId)
                    {
                        dataModels.get(i).setClick(true);
                        break;
                    }
                }
                adapter= new CustomAdapter(dataModels,getApplicationContext());
                listView.setAdapter(adapter);

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else {

            for(int i=0; i<dataModels.size(); i++)
            {
                if(i==0)
                {
                    dataModels.get(i).setClick(true);

                    try {

                        SelfExamId = dataModels.get(i).getExamId();
                        plan_list_array.clear();
                        loadQuestions();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                   /* Intent in = new Intent("SelfEvaluation");
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("SelfExamId", dataModels.get(i).getExamId());
                    in.putExtras(mBundle);
                    sendBroadcast(in);*/
                    break;
                }
            }
            adapter= new CustomAdapter(dataModels,getApplicationContext());

            listView.setAdapter(adapter);
        }

    }



    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }

        /*float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/

        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Bundle bundle = intent.getExtras();
                    SelfExamId = bundle.getInt("SelfExamId");
                    plan_list_array.clear();
                    loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("SelfEvaluation");

        registerReceiver(questionreceiver,intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
    }
    void setbackground(ImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    private void loadQuestionst(int SelfExamId) throws Exception {
        try {

            mark=0;
            descriptionval = db.getAllExamsDetailsusingExamId(SelfExamId, "Self-Evaluation");
            subject =db.getAllExamsDetailsusingExamSubject(SelfExamId, "Self-Evaluation");
           dateva= db.getAllExamsDetailsusingExamDate(SelfExamId, "Self-Evaluation");
            examname.setText(descriptionval);
            examdate.setText(dateva);
             subname.setText(subject);
            DatabaseHandler db = new DatabaseHandler(SelfEvaluationTotalExamList.this);

            List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(SelfExamId);

            for (QuestionDetails cn : questiondetailsList) {
                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                // Writing Contacts to log
                Log.d("Question3: ", log);
                Question = cn.getQuestion();
                QuestionNumber = cn.getQuestionNumber();
                OptionA = cn.getOptionA();
                OptionB = cn.getOptionB();
                OptionC = cn.getOptionC();
                OptionD = cn.getOptionD();
                CorrectAnswer = cn.getCorrectAnswer();
                QuestionID = cn.getQuestionID();
                StudentAnswer = cn.getStudentAnswer();
                IsCorrect = cn.getIsCorrect();
                ObtainedScore = cn.getObtainedScore();
                 mark = cn.getMark()+mark;

                System.out.println(mark+"mark");

                CorrectAnswer = cn.getCorrectAnswer();
                try {
                    plan_list_array.add(new QuestionPojo(Question, OptionA, OptionB, OptionC, OptionD, CorrectAnswer, StudentAnswer, StudName, "", "", "TotalNoQuestion", "StudentPhotPath", QuestionNumber));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            obtainedscoreval.setText("Marks Obtained: " + ObtainedScore + "/" + mark);

            selfevalutation_adapter = new SelfTestQuestionAdapter(SelfEvaluationTotalExamList.this, R.layout.self_asses_quesandans,
                    plan_list_array);
            self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
            mainLinearLayout.addView(linearLayout2);


        } catch (Exception e) {
            e.printStackTrace();
            //You'll need to add proper error handling here
        }
    }
    private void loadQuestions() throws Exception {
        try {


            descriptionval = db.getAllExamsDetailsusingExamId(SelfExamId, "Self-Evaluation");
            subject =db.getAllExamsDetailsusingExamSubject(SelfExamId, "Self-Evaluation");
            dateva= db.getAllExamsDetailsusingExamDate(SelfExamId, "Self-Evaluation");
            examname.setText(descriptionval);
            examdate.setText(dateva);
            subname.setText(subject);
            DatabaseHandler db = new DatabaseHandler(SelfEvaluationTotalExamList.this);

                List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(SelfExamId);

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);
                    Question =cn.getQuestion();
                    QuestionNumber = cn.getQuestionNumber();
                    OptionA =cn.getOptionA();
                    OptionB =cn.getOptionB();
                    OptionC =cn.getOptionC();
                    OptionD =cn.getOptionD();
                    CorrectAnswer = cn.getCorrectAnswer();
                    QuestionID= cn.getQuestionID();
                    StudentAnswer=  cn.getStudentAnswer();
                    IsCorrect=  cn.getIsCorrect();
                    ObtainedScore=  cn.getObtainedScore();

                    CorrectAnswer = cn.getCorrectAnswer();
                    try {
                        plan_list_array.add(new QuestionPojo(Question, OptionA, OptionB, OptionC, OptionD, CorrectAnswer, StudentAnswer, StudName, "", "", "TotalNoQuestion", "StudentPhotPath",QuestionNumber));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    mark = cn.getMark()+mark;

                }

            obtainedscoreval.setText("Marks Obtained: "+ObtainedScore+"/"+mark);

            selfevalutation_adapter = new SelfTestQuestionAdapter(SelfEvaluationTotalExamList.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
                mainLinearLayout.addView(linearLayout2);


        }
        catch (Exception e) {
            //You'll need to add proper error handling here
        }




//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }
/*    private void loadQuestions() throws Exception {
        String fileContent = new String();
        //Find the directory for the SD Card using the API
/*//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();
//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();
            br.close();
            System.out.println("fileContent.length()" + fileContent.length());
            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                JSONObject resultObject = null;
                JSONArray jsonArray = null;
                QuizWrapper newItemObject = null;
                try {
                    resultObject = new JSONObject(fileContent);
                    System.out.println("Testing the water " + resultObject.toString());
                    jsonArray = resultObject.optJSONArray("questions");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonChildNode = null;
                    try {
                        jsonChildNode = jsonArray.getJSONObject(i);
                        int id = 1;
                        String question = jsonChildNode.getString("question");
                        JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                        int correctAnswer = jsonChildNode.getInt("correctIndex");
                        try {
                            plan_list_array.add(new QuestionPojo(question, "", "", "", "", String.valueOf(correctAnswer), "1", "StudentName", "StudentRollNumber", "StudentID", "TotalNoQuestion", "StudentPhotPath"));

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                selfevalutation_adapter = new SelfTestQuestionAdapter(SelfEvaluationTotalExamList.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
                mainLinearLayout.addView(linearLayout2);

            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }*/

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }


    public void stratExamPopup() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.start_exam_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button2);
        yesBtn = (Button) dialog.findViewById(R.id.button3);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();



            }
        });
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(SelfEvaluationTotalExamList.this, QuizActivity.class);
                intent.putExtra("Examquestion", "SelfEvaluation");
                startActivity(intent);

                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}




