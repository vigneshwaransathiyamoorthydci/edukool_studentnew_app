package exam;

/**
 * Created by pratheeba on 5/5/2017.
 */
public class AspectReportPojo {

    int ReportID;
    int ExamID;
    int TopicID;
    String TopicName;
    int AspectID;
    String Aspect;

    public int getReportID() {
        return ReportID;
    }

    public void setReportID(int reportID) {
        ReportID = reportID;
    }

    public int getExamID() {
        return ExamID;
    }

    public void setExamID(int examID) {
        ExamID = examID;
    }

    public int getTopicID() {
        return TopicID;
    }

    public void setTopicID(int topicID) {
        TopicID = topicID;
    }

    public String getTopicName() {
        return TopicName;
    }

    public void setTopicName(String topicName) {
        TopicName = topicName;
    }

    public int getAspectID() {
        return AspectID;
    }

    public void setAspectID(int aspectID) {
        AspectID = aspectID;
    }

    public String getAspect() {
        return Aspect;
    }

    public void setAspect(String aspect) {
        Aspect = aspect;
    }

    public int getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(int questionID) {
        QuestionID = questionID;
    }

    public int getNoOfCorrectAnswers() {
        return NoOfCorrectAnswers;
    }

    public void setNoOfCorrectAnswers(int noOfCorrectAnswers) {
        NoOfCorrectAnswers = noOfCorrectAnswers;
    }

    public int getNoOfWrongAnswers() {
        return NoOfWrongAnswers;
    }

    public void setNoOfWrongAnswers(int noOfWrongAnswers) {
        NoOfWrongAnswers = noOfWrongAnswers;
    }

    int QuestionID;
    int NoOfCorrectAnswers;
    int NoOfWrongAnswers;
}
