package exam;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;

/**
 * Created by pratheeba on 4/21/2017.
 */


import android.app.Activity;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Utils.Utils;
import helper.RoundedImageView;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

public class StudentSelfAssesmentList extends Activity {

    private LinearLayout mainLinearLayout;
   // private RelativeLayout linearLayout1;
    private RelativeLayout linearLayout;
   // private RelativeLayout linearLayout2;
    private TextView quizQuestion,timer_text;
    private RadioGroup radioGroup;
    private RadioButton optionOne;
    private RadioButton optionTwo;
    private RadioButton optionThree;
    private RadioButton optionFour;
    private int currentQuizQuestion;
    private int quizCount;
    BroadcastReceiver questionreceiver;
    static JSONArray quesList;
    static JSONArray examDetailList;
    private int mQuestionIndex = 0;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    SelfTestQuestionAdapter selfevalutation_adapter;
    ExamGridAdapter exam_grid_adapter;
    private Dialog dialog;
    private TextView headerText;
    private TextView bodyText;
    private Button closeButton, yesBtn;
    private Button results;
    private Button layoutinputButton;
   // ListView listView,self_asses_ques_ans_list;
    ImageView download_exam;
    GridView exam_list_grid;
    ArrayList<DataModel> dataModels;
    ArrayList<NameValuePair> selfasses = new ArrayList<NameValuePair>();
    private static CustomAdapter adapter;
    Utils utils;
    View header;
    ProgressDialog dia;
    int ExamIDValue;
    DatabaseHandler db;
    ArrayList<String>bookshelf;
    ArrayList<String>bookshelfid;

    ImageView back;
    TextView stdname;
    Spinner subjectspinner;
    int batid;
    String studentname,profilename;
    RoundedImageView profileimage;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.seld_eveluation_header);

        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        download_exam= (ImageView) findViewById(R.id.download_exam);
        stdname= (TextView)findViewById(R.id.studentname);
        results = (Button) findViewById(R.id.results);
        profileimage= (RoundedImageView) findViewById(R.id.imageView6);
        subjectspinner= (Spinner) findViewById(R.id.subjectspinner);

        exam_list_grid= (GridView) findViewById(R.id.gridView);
        back= (ImageView) findViewById(R.id.back);
       /* linearLayout1 = (RelativeLayout) View.inflate(this,
                R.layout.student_self_asses_list, null);*/
        db = new DatabaseHandler(this);
       /* linearLayout = (RelativeLayout) View.inflate(this,
                R.layout.exams_grid_layout, null);*/
       /* linearLayout2 = (RelativeLayout) View.inflate(this,
                R.layout.self_asses_ques_ans_list, null);*/
        dataModels= new ArrayList<>();


        //dataModels=new ArrayList<String>();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //listView = (ListView) linearLayout1.findViewById(R.id.listView);
        //self_asses_ques_ans_list = (ListView) linearLayout2.findViewById(R.id.listView);

        //adapter= new CustomAdapter(dataModels,getApplicationContext());
       // listView.setAdapter(adapter);


        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());

        bookshelf=new ArrayList<>();
        bookshelfid=new ArrayList<>();

        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            studentname= tc.getString(tc.getColumnIndex("FirstName"));
            profilename= tc.getString(tc.getColumnIndex("PhotoFilename"));
        }

        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
            batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }


        Cursor subcursor=obj.retriveSubjects(batid);
        while(subcursor.moveToNext()){
            bookshelfid.add(subcursor.getString(subcursor.getColumnIndex("SubjectID")));
            bookshelf.add(subcursor.getString(subcursor.getColumnIndex("SubjectName")));
        }


        CustomSpinnerAdapter down=new CustomSpinnerAdapter(StudentSelfAssesmentList.this,android.R.layout.simple_spinner_item,bookshelf);
        // Customadapter adapter=new Customadapter(getApplicationContext(),gettable);
        // upper=new Upperspinner(this,android.R.layout.simple_spinner_item,gettable);
        subjectspinner.setAdapter(down);

        subjectspinner.setSelection(0);


        subjectspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    plan_list_array.clear();
                    loadExamList(bookshelfid.get(position));
                    exam_grid_adapter.notifyDataSetChanged();

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        stdname.setText(studentname.toUpperCase());

        if(profilename!=null)setbackground(profileimage,profilename);



        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(StudentSelfAssesmentList.this, SelfEvaluationTotalExamList.class);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        download_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // String login_str="UserName:"+username.getText().toString()+"|StudentId:"+password.getText().toString()+"|Function:DownloadSelfEvaluation";
                    String login_str = "UserName:" + "EK-001-S0000000001" + "|StudentId:" + "EK-001-S0000000001" + "|Function:DownloadSelfEvaluation";

                    selfasses.clear();
                    byte[] data;
                    try {
                        data = login_str.getBytes("UTF-8");
                        String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                        if (utils.hasConnection()) {
                            selfasses.clear();
                            selfasses.add(new BasicNameValuePair("WS", base64_register));

                           /* SelfEvaluationWS load_plan_list = new SelfEvaluationWS(StudentSelfAssesmentList.this, login);
                            load_plan_list.execute();*/
                        } else {
                            utils.Showalert();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
            if(curBrightnessValue>20) {
                float brightness = curBrightnessValue / (float) 255;
                WindowManager.LayoutParams lp = getWindow().getAttributes();


                lp.screenBrightness = brightness;
                getWindow().setAttributes(lp);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    plan_list_array.clear();
                   // loadExamList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("Exam");

        registerReceiver(questionreceiver,intent);
    }



    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
    }

/*

    class SelfEvaluationWS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public SelfEvaluationWS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(StudentSelfAssesmentList.this);
            dia.setMessage("GET IN");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               */
/* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*//*


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(StudentSelfAssesmentList.this);
                jsonResponseString = sr.getLogin(selfasses,
                        "http://api.schoolproject.dci.in/api/");
                   */
/* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*//*

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {



                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success")) {
                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(StudentSelfAssesmentList.this);
                            dia.setMessage("DOWNLOADING");
                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {





                                JSONArray Exam_arr=jObj.getJSONObject("examDetails").getJSONArray("exam");

                                for (int i = 0; i < Exam_arr.length(); i++) {
                                    JSONObject objexam=Exam_arr.getJSONObject(i);
                                    ExamIDVal = objexam.getString("ExamID");
                                    ExamID = Integer.parseInt(ExamIDVal);
                                    ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                                    ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                                    ExamCategory = objexam.getString("ExamCategory");
                                    ExamCode = objexam.getString("ExamCode");
                                    ExamDescription = objexam.getString("ExamDescription");
                                    BatchIDVal = objexam.getString("BatchID");
                                    ExamDurationVal = objexam.getString("ExamDuration");
                                    BatchID = Integer.parseInt(BatchIDVal);
                                    ExamDuration = Integer.parseInt(ExamDurationVal);

                                    SubjectIDVal = objexam.getString("SubjectID");
                                    Subject = objexam.getString("Subject");

                                    SubjectID = Integer.parseInt(SubjectIDVal);
                                    ExamTypeIDVal = objexam.getString("ExamTypeID");

                                    ExamType = objexam.getString("ExamType");

                                    // ExamDate = objexam.getString("ExamDate");

                                    ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   */
/* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*//*

                                    // String Questions = objexam.getString("Questions");

                                    int ExamSequence =0;
                                    int SchoolID =0;
                                    int IsResultPublished =0;
                                    int ExamShelfID =0;
                                    int ClassID =0;
                                    int TimeTaken = 0;
                                    int TotalScore =0;
                                    String DateAttended ="01/06/2017 10:00:00";
                                    db.addExamDetails(new ExamDetails(ExamID,ExamCategoryID,ExamCategory,ExamCode,ExamDescription,ExamSequence,ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                            BatchID, IsResultPublished,
                                            ExamShelfID,
                                            TimeTaken,
                                            DateAttended,
                                            TotalScore));
                                    List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

                                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                                    QuizWrapper newItemObject = null;

                                    for(int j = 0; j < jsonArrayquestion.length(); j++){
                                        JSONObject jsonChildNode = null;
                                        try {
                                            jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                            String QuestionIDval = jsonChildNode.getString("QuestionID");
                                            QuestionID = Integer.parseInt(QuestionIDval);
                                            TopicIDVal = jsonChildNode.getString("TopicID");
                                            if(TopicIDVal.equalsIgnoreCase("")){
                                                TopicID=1;
                                            }
                                            else{
                                                TopicID = Integer.parseInt(TopicIDVal);

                                            }

                                            // int TopicID=1;
                                            Topic = jsonChildNode.getString("Topic");
                                            AspectIDVal = jsonChildNode.getString("AspectID");
                                            if(AspectIDVal.equalsIgnoreCase("")){
                                                AspectID=1;
                                            }
                                            else{
                                                AspectID = Integer.parseInt(AspectIDVal);

                                            }
                                            Aspect = jsonChildNode.getString("Aspect");
                                            QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                            QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                            question= jsonChildNode.getString("Question");
                                            JSONArray options = jsonChildNode.getJSONArray("Options");
                                            CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                            CorrectAnswerVal = 1;
                                            MarkVal = jsonChildNode.getString("Mark");
                                            Mark = Integer.parseInt(MarkVal);

                                            NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                            NegativeMark = Integer.parseInt(NegativeMarkVal);
                                            String Created_on = "01/06/2017 10:00:00";
                                            String ModifiedOn = "01/06/2017 10:00:00";
                                            StudentAnswer ="";
                                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                                            if(options.length()>3){
                                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            }

                                            else if(options.length()>2){
                                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            }

                                            else if(options.length()>1){
                                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            }
                                            List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                                            List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                            Calendar calendar = Calendar.getInstance();
                                            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                            String strDate = mdformat.format(calendar.getTime());

                                            for (ExamDetails cn : examdetailsList) {
                                                String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                                                // Writing Contacts to log
                                                Log.d("Exam2: ", log);
                                            }

                                            for (QuestionDetails cn : questiondetailsList) {
                                                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                                // Writing Contacts to log
                                                Log.d("Question2: ", log);
                                            }

                                            jsonObject.add(newItemObject);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    // Adding child data

                                    // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                                    //jsonObject.add(newItemObject);

                                }

                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                           // startActivity(new Intent(StudentSelfAssesmentList.this,MainActivity.class));
                            finish();

                        }
                    }.execute();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                System.out.println(e.toString() + "zcx");
            }

        }
    }
*/


    void setbackground(ImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    private void loadExamList(String subjectID) {
        List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();

        for (QuestionDetails cn : questiondetailsList) {
            String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
            // Writing Contacts to log
            Log.d("Question: ", log);
        }
        List<ExamDetails> examdetails = db.GetAllExamFromServer(subjectID);

        if(examdetails.size()==0){
           Toast.makeText(getApplicationContext(), "No exams found",Toast.LENGTH_LONG).show();
        }
        else{
            for (ExamDetails cn : examdetails) {
                // dataModels.add(new DataModel(cn.getExamDescription(),cn.getExamID()));
                plan_list_array.add(new QuestionPojo(cn.getExamDescription(),cn.getExamID()));

            }
            exam_grid_adapter = new ExamGridAdapter(StudentSelfAssesmentList.this, R.layout.exams_grid_adapter,
                    plan_list_array);
            exam_list_grid.setAdapter(exam_grid_adapter);
            //mainLinearLayout.addView(linearLayout);


        }

          }



/*    private void loadQuestions() throws Exception {
        String fileContent = new String();
        //Find the directory for the SD Card using the API
/*//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();
//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();
            br.close();
            System.out.println("fileContent.length()" + fileContent.length());
            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                JSONObject resultObject = null;
                JSONArray jsonArray = null;
                QuizWrapper newItemObject = null;
                try {
                    resultObject = new JSONObject(fileContent);
                    System.out.println("Testing the water " + resultObject.toString());
                    jsonArray = resultObject.optJSONArray("questions");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonChildNode = null;
                    try {
                        jsonChildNode = jsonArray.getJSONObject(i);
                        int id = 1;
                        String question = jsonChildNode.getString("question");
                        JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                        int correctAnswer = jsonChildNode.getInt("correctIndex");
                        try {
                   plan_list_array.add(new QuestionPojo(question, "", "", "", "", String.valueOf(correctAnswer), "1", "StudentName", "StudentRollNumber", "StudentID", "TotalNoQuestion", "StudentPhotPath"));

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                selfevalutation_adapter = new SelfTestQuestionAdapter(StudentSelfAssesmentList.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
                mainLinearLayout.addView(linearLayout2);

            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);


//Set the text
        // tv.setText(text);

    }*/

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }


    public void stratExamPopup() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.start_exam_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button2);
        yesBtn = (Button) dialog.findViewById(R.id.button3);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();



            }
        });
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                    Intent intent = new Intent(StudentSelfAssesmentList.this, QuizActivity.class);
                    intent.putExtra("Examquestion", "SelfEvaluation");
                    intent.putExtra("ExamID", "SelfEvaluation");
                    startActivity(intent);

                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
    }




