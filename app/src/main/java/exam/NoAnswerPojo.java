package exam;

/**
 * Created by pratheeba on 5/24/2017.
 */
public class NoAnswerPojo {

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public String getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(String questionNumber) {
        QuestionNumber = questionNumber;
    }

    String selectedAnswer="-1";
    String QuestionNumber;
}
