package calendar;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CalendarHelper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import Utils.Service;
import Utils.Utils;
import Utils.*;
import helper.RoundedImageView;

import hirondelle.date4j.DateTime;
import com.dci.edukool.student.CalendarPojo;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;


/**
 * Created by kirubakaranj on 5/26/2017.
 */

public class Calendaractivity extends AppCompatActivity {

    LinearLayout calendarfrag;
    private CaldroidFragment caldroidFragment;
    ListView firstlist_view;
    FirstListAdapter fadapter ;
    ArrayList<Calendarpojo> catelist;
    SqliteOpenHelperDemo db;

    ListView desc_view;
    SecondListAdapter second_adapter ;
    ArrayList<Calendarpojo> desclist;
    ImageView back,down;
    Button today;
    SharedPreferences pref;
    TextView staffclass,todat,notsch;
    //Staffloingdetails logindetails;
    RoundedImageView profileimage;
    String profile,stdname;

    Utils utils;
    int stdid;
    String stdlogin_id,calenderpath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.calender);
        notsch=(TextView)findViewById(R.id.notsch);
        back=(ImageView)findViewById(R.id.back);
        today=(Button)findViewById(R.id.today);
        todat=(TextView)findViewById(R.id.todat);
        staffclass=(TextView)findViewById(R.id.staffclas);
        profileimage=(RoundedImageView)findViewById(R.id.profileimage);
        down= (ImageView) findViewById(R.id.handrise);


        utils=new Utils(Calendaractivity.this);
        pref=getSharedPreferences("student",MODE_PRIVATE);
        calenderpath=pref.getString("calender","");


        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }

       // getSupportActionBar().hide();
        caldroidFragment = new CaldroidSampleCustomFragment();

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();



        firstlist_view=(ListView)findViewById(R.id.calendarevent);
        desc_view=(ListView)findViewById(R.id.desriptionview);

        catelist=new ArrayList<>();
        db=new SqliteOpenHelperDemo(this);

        Cursor stdcur = db.retrive("tblStudent");
        while (stdcur.moveToNext()) {
            profile = stdcur.getString(stdcur.getColumnIndex("PhotoFilename"));
            stdname = stdcur.getString(stdcur.getColumnIndex("FirstName"));
            stdid =stdcur.getInt(stdcur.getColumnIndex("StudentID"));
            stdlogin_id =stdcur.getString(stdcur.getColumnIndex("StudentLoginID"));
        }


        //  logindetails=db.staffdetails();
        setbackground(profileimage,profile);

        Cursor c=db.retriveCategoryType();
        while(c.moveToNext()){
            Calendarpojo pojo=new Calendarpojo();
            pojo.setCategory(c.getString(c.getColumnIndex("Name")));
            pojo.setCategoryIcon(c.getString(c.getColumnIndex("Image")));
            catelist.add(pojo);
        }

         fadapter = new FirstListAdapter(this,catelist);
         firstlist_view.setAdapter(fadapter);





         desclist=new ArrayList<>();

        // desclist=db.retriveCalendar();
       // Log.d("listsize",desclist.size()+"");
         second_adapter = new SecondListAdapter(this,db.retriveCalendar());

         desc_view.setAdapter(second_adapter);




        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime  today = CalendarHelper.convertDateToDateTime(new Date());
                moveTo(today);

                todat.setText(formatdate(getToday().toString()));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Calendaractivity.this.finish();
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(!pref.getBoolean("classtime",false)) {
                new validateUserTask().execute("");
            }else{
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.session),Toast.LENGTH_LONG).show();
            }
            }
        });

      try {
          staffclass.setText(stdname.toUpperCase());
      }catch(Exception e){}
    }


    private class validateUserTask extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(Calendaractivity.this);
            dia.setMessage("DOWNLOADING...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

            String userid = "Function:DownloadStudentEvents|UserName:" + stdlogin_id + "|StudentId:"+ stdid;

            String res = null;
            byte[] data;

            try {
                data = userid.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                if (utils.hasConnection()) {
                    postParameters.clear();
                    postParameters.add(new BasicNameValuePair("WS", base64_register));
                    Service sr = new Service(Calendaractivity.this);
                    res = sr.getLogin(postParameters,
                            Url.base_url);
                   /* response = CustomHttpClient.executeHttpPost(params[0], postParameters);*/
                    // res = response.toString();
                    Log.e("response", "resoibse" + res + ",,");
                } else {
                    utils.Showalert();
                }
                // res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                if (dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {

            if(dia.isShowing())
                dia.cancel();

            try {

                JSONObject jObj = new JSONObject(result);
                String statusCode = jObj.getString("StatusCode");

                if (statusCode.toString().equalsIgnoreCase("200")) {
                ArrayList<CalendarPojo> calendarlist = new ArrayList<CalendarPojo>();

                for (int i = 0; i < jObj.getJSONArray("Calender").length(); i++) {

                    CalendarPojo calendarPojo = new CalendarPojo();

                    Cursor c=db.retriveIsCalendarEventID(Integer.parseInt(jObj.getJSONArray("Calender").getJSONObject(i).getString("ID")));

                    if(c.getCount()>0==false) {
                        calendarPojo.setEventID(jObj.getJSONArray("Calender").getJSONObject(i).getString("ID"));
                        calendarPojo.setEventName(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventName"));
                        calendarPojo.setEventDescription(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventDescription"));
                        calendarPojo.setEventStartDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventStartDate"));
                        calendarPojo.setEventEndDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventEndDate"));
                        calendarPojo.setCategory(jObj.getJSONArray("Calender").getJSONObject(i).getString("Category"));
                        calendarPojo.setCategoryID(Integer.parseInt(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryID")));

                        String calenderImage = jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage");
                        String calfile = jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").substring(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").lastIndexOf("/") + 1, jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").length());
                        String cal_imagepath = calenderpath + "/" + calenderImage.substring(calenderImage.lastIndexOf("/") + 1, calenderImage.length());
                        try {
                            downloadfile(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage"), calenderpath, calfile);
                        } catch (Exception e) {
                        }

                        calendarPojo.setCategoryIcon(cal_imagepath);
                        calendarlist.add(calendarPojo);
                    }
                    if(c.getCount()>0){

                        calendarPojo.setEventID(jObj.getJSONArray("Calender").getJSONObject(i).getString("ID"));
                        calendarPojo.setEventName(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventName"));
                        calendarPojo.setEventDescription(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventDescription"));
                        calendarPojo.setEventStartDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventStartDate"));
                        calendarPojo.setEventEndDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventEndDate"));
                        calendarPojo.setCategory(jObj.getJSONArray("Calender").getJSONObject(i).getString("Category"));
                        calendarPojo.setCategoryID(Integer.parseInt(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryID")));

                        String calenderImage = jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage");
                        String calfile = jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").substring(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").lastIndexOf("/") + 1, jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").length());
                        String cal_imagepath = calenderpath + "/" + calenderImage.substring(calenderImage.lastIndexOf("/") + 1, calenderImage.length());
                        try {
                            downloadfile(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage"), calenderpath, calfile);
                        } catch (Exception e) {
                        }

                        calendarPojo.setCategoryIcon(cal_imagepath);

                        db.UpdateCalendarDownload(calendarPojo);
                    }
                }

                if(calendarlist.size()!=0)
               db.InsertCalender(calendarlist);

            }//status code=200

            }
            catch(Exception e){

            }

            if(db.retriveCalendar().size()!=0) {

                caldroidFragment = new CaldroidSampleCustomFragment();

                FragmentTransaction t = getSupportFragmentManager().beginTransaction();
                t.replace(R.id.calendar, caldroidFragment);
                t.commit();

                second_adapter = new SecondListAdapter(Calendaractivity.this, db.retriveCalendar());

                desc_view.setAdapter(second_adapter);

            }

            new validateUserTask2(result).execute("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
    }

    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response,statusres;

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(Calendaractivity.this);
            dia.setMessage("DOWNLOADING...");
            dia.setCancelable(false);
            dia.show();}

        validateUserTask2(String statusres){
            this.statusres=statusres;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:StudentSyncComplete|StudentID:" + stdid + "|UserName:" + stdlogin_id + "|Type:event";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(Calendaractivity.this);

                        res = sr.getLogin(postParameters,
                                Url.base_url);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                }
                else{
                    Log.e("error","ss");
                }

            }
            catch(Exception e){
                if(dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(dia.isShowing())
                dia.cancel();
            try {
                JSONObject jObj = new JSONObject(result);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {
                    Toast.makeText(getApplicationContext(),"Calendar data downloaded successfully.",Toast.LENGTH_LONG).show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Downloading data failed.",Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {

            }
            //Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();

        }//close onPostExecute
    }// close validateUserTask


    @Override
    public void onBackPressed() {
        // your code.
    }

    public void setList (ArrayList<Calendarpojo> desclist2){
        Log.e("sizelist",""+desclist2.size());

        if(desclist2.size()!=0) {
            desc_view.setVisibility(View.VISIBLE);
            notsch.setVisibility(View.GONE);
            second_adapter = new SecondListAdapter(this, desclist2);
            desc_view.setAdapter(second_adapter);
        }

    }

    public void setList2(){
            desc_view.setVisibility(View.GONE);
            notsch.setVisibility(View.VISIBLE);
    }

   public void setDate(DateTime datetime){
       String dateString=datetime.toString();
       String[] separated = dateString.split(" ");

       todat.setText(formatdate(separated[0]));
   }

    public void moveTo(DateTime datetime){
        caldroidFragment.moveToDateTime(datetime);


        String dateString=datetime.toString();
        String[] separated = dateString.split(" ");

        Cursor c = db.retriveStartDate(separated[0]);
        ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                Calendarpojo pojo = new Calendarpojo();
                pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
                pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
                pojo.setCategory(c.getString(c.getColumnIndex("Category")));
                pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
                pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
                pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
                pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
                list.add(pojo);
            }
        }

        Cursor end = db.retriveEnddate(separated[0]);
        if (end.getCount() > 0) {
            while (end.moveToNext()) {
                Calendarpojo pojo = new Calendarpojo();
                pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
                pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
                pojo.setCategory(end.getString(end.getColumnIndex("Category")));
                pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
                pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
                pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
                pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
                list.add(pojo);
            }
        }
        if (list.size() != 0) {
          setList(list);
        } else if (list.size() == 0) {
            setList2();
        }
     }

    public DateTime getToday() {
        DateTime today=null;
        if (today == null) {
            today = CalendarHelper.convertDateToDateTime(new Date());
        }
        return today;
    }
    public String formatdate(String fdate)
    {
        String datetime=null;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat d= new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date convertedDate = inputFormat.parse(fdate);
            datetime = d.format(convertedDate);

        }catch (ParseException e)
        {

        }
        return  datetime;


    }

    public static String headerdate()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    void setbackground(RoundedImageView view, String filepath)
    {
        try
        {


        File imgFile=new File(filepath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
        }
        catch (Exception e)
        {

        }
    }

    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

}
