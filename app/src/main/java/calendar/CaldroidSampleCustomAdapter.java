package calendar;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;


import hirondelle.date4j.DateTime;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {
	ArrayList<Calendarpojo> calendarlist;
	public CaldroidSampleCustomAdapter(Context context, int month, int year,
									   Map<String, Object> caldroidData,
									   Map<String, Object> extraData,ArrayList<Calendarpojo> calendarlist) {
		super(context, month, year, caldroidData, extraData);
		this.calendarlist=calendarlist;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cellView = convertView;

		ViewHolder viewHolder=null;
		// For reuse
		if (cellView == null) {
			cellView = inflater.inflate(R.layout.custom_cell, null);
			viewHolder = new ViewHolder(cellView);
			cellView.setTag(viewHolder);
		}else {
				viewHolder = (ViewHolder) cellView.getTag();
		}

		int topPadding = cellView.getPaddingTop();
		int leftPadding = cellView.getPaddingLeft();
		int bottomPadding = cellView.getPaddingBottom();
		int rightPadding = cellView.getPaddingRight();

	/*	TextView tv1 = (TextView) cellView.findViewById(R.id.tv1);
		TextView tv2 = (TextView) cellView.findViewById(R.id.tv2);
		TextView tv3 = (TextView) cellView.findViewById(R.id.tv3);

		ImageView holiday = (ImageView) cellView.findViewById(R.id.holiday);
		ImageView exam = (ImageView) cellView.findViewById(R.id.exams);
		ImageView event = (ImageView) cellView.findViewById(R.id.events);
		ImageView competition = (ImageView) cellView.findViewById(R.id.competition);
*/

		viewHolder.tv1.setTextColor(Color.BLACK);

		// Get dateTime of this cell
		DateTime dateTime = this.datetimeList.get(position);
		viewHolder.layoutParent.setTag(position);
		//	Log.e("datetime",""+dateTime);
		Resources resources = context.getResources();

		// Set color of the dates in previous / next month
		if (dateTime.getMonth() != month) {
			viewHolder.tv1.setTextColor(resources
					.getColor(com.caldroid.R.color.caldroid_darker_gray));
		}

		boolean shouldResetDiabledView = false;
		boolean shouldResetSelectedView = false;

		// Customize for disabled dates and date outside min/max dates
		if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

			viewHolder.tv1.setTextColor(CaldroidFragment.disabledTextColor);
			if (CaldroidFragment.disabledBackgroundDrawable == -1) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.disable_cell);
			} else {
				cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
			}

			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.red_border_gray_bg);
			}

		} else {
			shouldResetDiabledView = true;
		}

		// Customize for selected dates
		if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
			cellView.setBackgroundColor(resources
					.getColor(com.caldroid.R.color.caldroid_sky_blue));

			viewHolder.tv1.setTextColor(Color.BLACK);

		} else {
			shouldResetSelectedView = true;
		}

		if (shouldResetDiabledView && shouldResetSelectedView) {
			// Customize for today
			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.red_border);
			} else {
				cellView.setBackgroundResource(com.caldroid.R.drawable.cell_bg);
			}
		}



		SqliteOpenHelperDemo db=new SqliteOpenHelperDemo(context);

		String dateString=dateTime.toString();
		String[] separated = dateString.split(" ");
  Boolean chk=false,modi=false,oth=false;
		for(int i=0;i<calendarlist.size();i++) {
			//Log.e("size",calendarlist.size()+"");
			Log.e("eventst",calendarlist.get(i).getEventstart());
			Log.e("eveend",calendarlist.get(i).getEventend());

			Date beforedate=null;
			Date enddate=null;
			Date comparedate=null;
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
			try {
				beforedate = format.parse(calendarlist.get(i).getEventstart());
				enddate = format.parse(calendarlist.get(i).getEventend());
				comparedate = format.parse(separated[0]);
			}
			catch (Exception e)
			{
				Log.e("issuedate",""+e.toString());
			}


			if (beforedate.compareTo(comparedate)==0) {
				///Log.e("evid",calendarlist.get(i).getEventid()+"");
				viewHolder.tv2.setText(calendarlist.get(i).getEventstart());
                 chk=true;

				String catname="";

				int id=calendarlist.get(i).getCategoryID();

				Cursor typecur=db.retriveCalendarType(id);
				while(typecur.moveToNext()){
					catname=typecur.getString(typecur.getColumnIndex("Name"));
//					typecur.getString(typecur.getColumnIndex("Image"));
				}
				typecur.close();
				if (catname.equalsIgnoreCase("holiday")) {
					viewHolder.holiday.setBackgroundResource(R.drawable.mholiday);
				}
				if (catname.equalsIgnoreCase("Competition")) {
					viewHolder.competition.setBackgroundResource(R.drawable.mcompetition);
				}
				if (catname.equalsIgnoreCase("Event")) {
					viewHolder.event.setBackgroundResource(R.drawable.mevent);
				}
				if (catname.equalsIgnoreCase("Examination")) {
					viewHolder.exam.setBackgroundResource(R.drawable.assignment);
				}
				if( catname.equalsIgnoreCase("Assignment")) {
					viewHolder.assign.setBackgroundResource(R.drawable.assignment);
				}
			}
			else{
				if(!chk && i==calendarlist.size()-1){
				viewHolder.tv2.setText("");
				}
			}




   /* Log.e("evenend",calendarlist.get(7).getEventend()+"");
 Log.e("category",calendarlist.get(7).getCategory()+"");*/



			// }
			if(comparedate.after(beforedate) && comparedate.before(enddate)){
				Log.e("evid", calendarlist.get(i).getEventend() + "");
				viewHolder.tv2.setText(calendarlist.get(i).getEventstart());
				viewHolder.tv3.setText(calendarlist.get(i).getEventend());
				oth=true;
                   Log.e("afterdate",separated[0]);
				String catname="";
				int id=calendarlist.get(i).getCategoryID();

				Cursor typecur=db.retriveCalendarType(id);
				while(typecur.moveToNext()){
					catname=typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
				}
				typecur.close();
				if (catname.equalsIgnoreCase("holiday")) {
					viewHolder.holiday.setBackgroundResource(R.drawable.mholiday);
				}
				if (catname.equalsIgnoreCase("Competition")) {
					viewHolder.competition.setBackgroundResource(R.drawable.mcompetition);
				}
				if (catname.equalsIgnoreCase("event")) {
					viewHolder.event.setBackgroundResource(R.drawable.mevent);
				}
				if (catname.equalsIgnoreCase("Examination")) {
					viewHolder.exam.setBackgroundResource(R.drawable.mexam);
				}
				if (catname.equalsIgnoreCase("Assignment")) {
					viewHolder.assign.setBackgroundResource(R.drawable.mexam);
				}

			}
			else{
				if(!chk && !oth && i==calendarlist.size()-1){
					viewHolder.tv2.setText("");
					}
			}

   /* Log.e("evenend",calendarlist.get(7).getEventend()+"");I9XZ
	Log.e("category",calendarlist.get(7).getCategory()+"");*/
			if (enddate.compareTo(comparedate)==0) {
				Log.e("evid",calendarlist.get(i).getEventend()+"");
				viewHolder.tv3.setText(calendarlist.get(i).getEventend());
                modi=true;

				String catname="";

				int id=calendarlist.get(i).getCategoryID();

				Cursor typecur=db.retriveCalendarType(id);
				while(typecur.moveToNext()){
					catname=typecur.getString(typecur.getColumnIndex("Name"));
//					typecur.getString(typecur.getColumnIndex("Image"));
				}
				typecur.close();
				if (catname.equalsIgnoreCase("holiday")) {
					viewHolder.holiday.setBackgroundResource(R.drawable.mholiday);
				}
				if (catname.equalsIgnoreCase("Competition")) {
					viewHolder.competition.setBackgroundResource(R.drawable.mcompetition);
				}
				if (catname.equalsIgnoreCase("event")) {
					viewHolder.event.setBackgroundResource(R.drawable.mevent);
				}
				if (catname.equalsIgnoreCase("Examination")) {
					viewHolder.exam.setBackgroundResource(R.drawable.assignment);
				}
				if (catname.equalsIgnoreCase("Assignment")) {
					viewHolder.assign.setBackgroundResource(R.drawable.assignment);
				}
			}else{
				if(!chk && !modi &&  !oth && i==calendarlist.size()-1){
				viewHolder.tv3.setText("");
				viewHolder.holiday.setBackgroundResource(0);
				viewHolder.competition.setBackgroundResource(0);
				viewHolder.event.setBackgroundResource(0);
				viewHolder.exam.setBackgroundResource(0);
				viewHolder.assign.setBackgroundResource(0);}
			}


		}

		viewHolder.tv1.setText("" + dateTime.getDay());


		if (dateTime.equals(getToday())) {
			ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();

			if(viewHolder.tv2.getText().toString()!=null || viewHolder.tv2.getText().toString()!="") {
				Cursor c = db.retriveStartDate(viewHolder.tv2.getText().toString());


				if (c.getCount() > 0) {
					while (c.moveToNext()) {
						Calendarpojo pojo = new Calendarpojo();
						pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
						pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
						pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
						pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
						pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));

						int id=c.getInt(c.getColumnIndex("CategoryID"));
						pojo.setCategoryID(id);
						Cursor typecur=db.retriveCalendarType(id);
						while(typecur.moveToNext()){
							pojo.setCategory(typecur.getString(typecur.getColumnIndex("Name")));

							pojo.setCategoryIcon(typecur.getString(typecur.getColumnIndex("Image")));
						}
						//pojo.setCategory(c.getString(c.getColumnIndex("Category")));
					//	pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));


						list.add(pojo);
					}

				}
			}
			if(!viewHolder.tv2.getText().toString().equalsIgnoreCase(viewHolder.tv3.getText().toString())) {

				if (viewHolder.tv3.getText().toString() != null || viewHolder.tv3.getText().toString() != "") {
					Cursor end = db.retriveEnddate(viewHolder.tv3.getText().toString());
					if (end.getCount() > 0) {
						while (end.moveToNext()) {
							Calendarpojo pojo = new Calendarpojo();
							pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
							pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
							//pojo.setCategory(end.getString(end.getColumnIndex("Category")));
							//	pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
							pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
							pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
							pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));

							int id = end.getInt(end.getColumnIndex("CategoryID"));
							pojo.setCategoryID(id);
							Cursor typcur = db.retriveCalendarType(id);
							while (typcur.moveToNext()) {
								pojo.setCategory(typcur.getString(typcur.getColumnIndex("Name")));

								pojo.setCategoryIcon(typcur.getString(typcur.getColumnIndex("Image")));
							}
							list.add(pojo);
						}
					}


				}
			}
			if (list.size() != 0) {
				((Calendaractivity) context).setList(list);
			} else if (list.size() == 0) {
				((Calendaractivity) context).setList2();
			}
		}
		/*tv2.setText(""+);*/

		viewHolder.layoutParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView stdate=(TextView)v.findViewById(R.id.tv2);
				TextView enddate=(TextView)v.findViewById(R.id.tv3);

				((Calendaractivity) context).setDate(datetimeList.get((int)v.getTag()));

				SqliteOpenHelperDemo db=new SqliteOpenHelperDemo(context);
				ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();

				if(stdate.getText().toString()!=null || stdate.getText().toString()!="") {
					Cursor c = db.retriveStartDate(stdate.getText().toString());


					if (c.getCount() > 0) {
						while (c.moveToNext()) {
							Calendarpojo pojo = new Calendarpojo();
							pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
							pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
						//	pojo.setCategory(c.getString(c.getColumnIndex("Category")));
						//	pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
							pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
							pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
							pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));

							int id=c.getInt(c.getColumnIndex("CategoryID"));
							pojo.setCategoryID(id);
							Cursor typecur=db.retriveCalendarType(id);
							while(typecur.moveToNext()){
								pojo.setCategory(typecur.getString(typecur.getColumnIndex("Name")));

								pojo.setCategoryIcon(typecur.getString(typecur.getColumnIndex("Image")));
							}
							list.add(pojo);
						}

					}
				}
                if(!stdate.getText().toString().equalsIgnoreCase(enddate.getText().toString())) {
					if (enddate.getText().toString() != null || enddate.getText().toString() != "") {
						Cursor end = db.retriveEnddate(enddate.getText().toString());
						if (end.getCount() > 0) {
							while (end.moveToNext()) {
								Calendarpojo pojo = new Calendarpojo();
								pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
								pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
								//	pojo.setCategory(end.getString(end.getColumnIndex("Category")));
								//	pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
								pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
								pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
								pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));

								int id = end.getInt(end.getColumnIndex("CategoryID"));
								pojo.setCategoryID(id);
								Cursor typcur = db.retriveCalendarType(id);
								while (typcur.moveToNext()) {
									pojo.setCategory(typcur.getString(typcur.getColumnIndex("Name")));

									pojo.setCategoryIcon(typcur.getString(typcur.getColumnIndex("Image")));
								}
								list.add(pojo);
							}
						}


					}
				}//stdate not equal to enddate condition


				if (list.size() != 0) {
					((Calendaractivity) context).setList(list);
				} else if (list.size() == 0) {
					((Calendaractivity) context).setList2();
				}
    /*else {
			((Calendaractivity) context).setList2();
		}*/


			}
		});




		/*if (dateTime.equals(getToday())) {
			// set datetime onclick calendar
			((Calendaractivity) context).setDate(dateTime);
			//end
			Cursor c = db.retriveStartDate(separated[0]);
			ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
			if (c.getCount() > 0) {
				while (c.moveToNext()) {
					Calendarpojo pojo = new Calendarpojo();
					pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
					pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
					pojo.setCategory(c.getString(c.getColumnIndex("Category")));
					pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
					pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
					pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
					pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
					list.add(pojo);
				}
			}

			Cursor end = db.retriveEnddate(separated[0]);
			if (end.getCount() > 0) {
				while (end.moveToNext()) {
					Calendarpojo pojo = new Calendarpojo();
					pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
					pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
					pojo.setCategory(end.getString(end.getColumnIndex("Category")));
					pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
					pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
					pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
					pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
					list.add(pojo);
				}
			}
			if (list.size() != 0) {
				((Calendaractivity) context).setList(list);
			} else if (list.size() == 0) {
				((Calendaractivity) context).setList2();
			}

		}*/
		// Somehow after setBackgroundResource, the padding collapse.
		// This is to recover the padding
		cellView.setPadding(leftPadding, topPadding, rightPadding,
				bottomPadding);

		// Set custom color if required
		setCustomResources(dateTime, cellView, viewHolder.tv1);

		return cellView;
	}

	private class ViewHolder {

		TextView tv1,tv2,tv3;
		RelativeLayout rel;
		ImageView holiday,exam,event,competition,assign;
		LinearLayout layoutParent;

		public ViewHolder(View view) {

			tv1 = (TextView) view.findViewById(R.id.tv1);
			tv2 = (TextView) view.findViewById(R.id.tv2);
			tv3 = (TextView) view.findViewById(R.id.tv3);
			layoutParent = (LinearLayout) view.findViewById(R.id.parentlayout);

			holiday = (ImageView) view.findViewById(R.id.holiday);
			exam = (ImageView) view.findViewById(R.id.exams);
			event = (ImageView) view.findViewById(R.id.events);
			competition = (ImageView) view.findViewById(R.id.competition);
			assign = (ImageView) view.findViewById(R.id.assign);



		}
	}
}
