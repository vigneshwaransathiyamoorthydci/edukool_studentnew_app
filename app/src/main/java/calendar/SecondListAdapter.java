package calendar;

/**
 * Created by kirubakaranj on 5/26/2017.
 */

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;


public class SecondListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Calendarpojo> items;
    SqliteOpenHelperDemo obj;

    public SecondListAdapter(Context context, ArrayList<Calendarpojo> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.desc_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

         Calendarpojo currentItem = (Calendarpojo) getItem(position);
    try {

        if (currentItem.getCategory().equalsIgnoreCase("holiday")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mholiday);
        }
        if (currentItem.getCategory().equalsIgnoreCase("Competition")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mcompetition);
        }
        if (currentItem.getCategory().equalsIgnoreCase("event")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mevent);
        }

        if(currentItem.getCategory().equalsIgnoreCase("Examination")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mexam);
        }
        if(currentItem.getCategory().equalsIgnoreCase("Assignment")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.assignment);
        }

    }
    catch(NullPointerException e){

    }
        viewHolder.event.setText(currentItem.getEventname());
//        Log.e("secondlist",currentItem.getEventname());

        viewHolder.desc.setText(Html.fromHtml(currentItem.getEventDesc()));
        viewHolder.dat.setText(formatdate(currentItem.getEventstart())+" To "+formatdate(currentItem.getEventend()));
     //   viewHolder.dat.setText(currentItem.getEventstart()+"-"+currentItem.getEventend());
        //  viewHolder.stdname.setText(currentItem.get());

      /*  viewHolder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //showPopup(msg.getText().toString(),stdname.getText().toString(),content.getText().toString(),dat.getText().toString());
            }
        });
*/

        return convertView;
    }

    public String formatdate(String fdate)
    {
        String datetime=null;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat d= new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date convertedDate = inputFormat.parse(fdate);
            datetime = d.format(convertedDate);

        }catch (ParseException e)
        {

        }
        return  datetime;


    }
    //ViewHolder inner class
    private class ViewHolder {


        TextView msg, event, desc, dat;
        RelativeLayout rel;
        ImageView cateicon;

        public ViewHolder(View view) {

            event = (TextView) view.findViewById(R.id.event);
            desc = (TextView) view.findViewById(R.id.desc);
            cateicon = (ImageView) view.findViewById(R.id.cateicon);
            dat = (TextView) view.findViewById(R.id.dat);
            // rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }


}