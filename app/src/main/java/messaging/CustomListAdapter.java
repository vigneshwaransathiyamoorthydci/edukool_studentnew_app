package messaging;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;



import java.util.ArrayList;

import com.dci.edukool.student.R;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class CustomListAdapter extends BaseAdapter {
    private Context context;
    ArrayList<Item> items;
    int i=0;
    public CustomListAdapter(Context context,   ArrayList<Item> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_list_view_row_items, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
            if(i%2==0){
                viewHolder.rel.setBackgroundColor(Color.parseColor("#553276"));
                i++;
            }
            else if(i%2==1){
                viewHolder.rel.setBackgroundColor(Color.parseColor("#73499B"));
                i++;
                          }
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Item currentItem = (Item) getItem(position);

        viewHolder.subname.setText(currentItem.getSubjectname());

        viewHolder.subid.setText(""+currentItem.getSubjectid());

        return convertView;
    }

    //ViewHolder inner class
    private class ViewHolder {
        CheckBox check;
        TextView subname,subid,stfid;
        RelativeLayout rel;
        TextView stdid;



        public ViewHolder(View view) {
         //   check = (CheckBox)view.findViewById(R.id.check);
            subname = (TextView) view.findViewById(R.id.subjectnam);
            subid = (TextView) view.findViewById(R.id.subjectid);
            stfid = (TextView) view.findViewById(R.id.staffid);
            rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }
}
