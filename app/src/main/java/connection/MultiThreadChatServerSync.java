

package connection;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import helper.Filemessage;
import com.dci.edukool.student.SqliteOpenHelperDemo;


/**
 *
 * @author mohammed
 */

// the Server class
public class MultiThreadChatServerSync {
   // The server socket.
  private static ServerSocket serverSocket = null;
  // The client socket.
  private static Socket clientSocket = null;
int portnumber =1370;

  private static final int maxClientsCount = 10;
  Filemessage msg;
SharedPreferences pref;
  String message_str;
  SqliteOpenHelperDemo obj;
  private static final int BUFFER_SIZE = 4096;
  public MultiThreadChatServerSync(SharedPreferences pref,SqliteOpenHelperDemo obj)

  {
    this.pref=pref;
    this.obj=obj;
new Thread(new Runnable() {
  @Override
  public void run() {

    try {
      serverSocket = new ServerSocket(portnumber);
    } catch (IOException e) {
      System.out.println(e);
    }

    while (true) {
      try {
        clientSocket = serverSocket.accept();
        int i = 0;
          Runnable r = new MyThreadHandler(clientSocket);
          Thread t = new Thread(r);
          t.start();

      } catch (IOException e) {
        System.out.println(e);
      }
    }

  }
}).start();

  }
  public void setmsg(Filemessage msg)
  {
    this.msg=msg;
  }







  private  class MyThreadHandler implements Runnable {
    private Socket socket;

    MyThreadHandler(Socket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
     /* clients++;
      System.out.println(clients + " JSONClient(s) connected on port: " + socket.getPort());
*/
      try {
        // For JSON Protocol
        JSONObject jsonObject = receiveJSON();
        //sendJSON(jsonObject);

      } catch (Exception e) {
        e.printStackTrace();

      } finally {
        /*try {
         // closeSocket();
        } catch (IOException e) {
          e.printStackTrace();
        }*/
      }
    }

    public void closeSocket() throws IOException {
      socket.close();
    }


    /**
     * use the JSON Protocol to receive a json object as
     * String from the client and reconstructs that object
     * @return JSONObejct with the same state (data) as
     * the JSONObject the client sent as a String msg.
     * @throws IOException
     */
    public JSONObject receiveJSON() throws IOException {



      int bytesRead;
      int current = 0;


      /*InputStream in = socket.getInputStream();

      DataInputStream clientData = new DataInputStream(in);

      String fileName = clientData.readUTF();
      File file = new File(
              Environment.getExternalStorageDirectory(),
              fileName);
      OutputStream output = new FileOutputStream(file);
      long size = clientData.readLong();
      byte[] buffer = new byte[1024];
      while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
      {
        output.write(buffer, 0, bytesRead);
        size -= bytesRead;
      }


      // Closing the FileOutputStream handle
      in.close();
      clientData.close();
      output.close();*/
      //msg.onMessageReceived("file recieved");




     /* InputStream in = socket.getInputStream();

      DataInputStream clientData = new DataInputStream(in);
      StringBuffer inputLine = new StringBuffer();


      int count = clientData.readInt();*/
      String message=getMessageFromTheConnection(socket);
      if(message.length()>0)
      {
        String split[]=message.split("\\@");
        if(message.startsWith("Filesharing")) {
          msg.onMessageReceived("Filesharing");
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          Runnable runclint = new Receivefile(split[1], Long.parseLong(split[2]),split[3],split[4],split[5], socket.getInetAddress().toString(),split[6]);
          Thread recf = new Thread(runclint);
          recf.start();
        }
        else if(message.startsWith("Timetable"))
        {
          msg.onMessageReceived("Filesharing");

          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          Runnable runclint = new Timetable(Integer.parseInt(split[1]), Integer.parseInt(split[2]),split[3],Long.parseLong(split[4]));
          Thread recf = new Thread(runclint);
          recf.start();
        }
        else if(message.startsWith("Whiteboardnotes"))
        {
          Runnable runclint = new whiteboard(split[1],Long.parseLong(split[2]));
          Thread recf = new Thread(runclint);
          recf.start();
        }
        else if(message.startsWith("fileopen"))
        {
          msg.onMessageReceived(message);
          /*try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }*/

        }
        else if(message.startsWith("Filemove"))
        {
          msg.onMessageReceived(message);
         /* try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }*/

        }
        else if(message.startsWith("FileClose"))
        {
          msg.onMessageReceived(message);
        /*  try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }*/

        }
        else if(message.contains("ExamResult"))
        {
          msg.onMessageReceived(message);

        }
        else if(message.startsWith("Examquestion"))
        {

          msg.onMessageReceived("Filesharing");
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          try {
            String exam_split[] = message.split("\\@");
            JSONObject array = new JSONObject(exam_split[1]);
            String status = array.getString("status");
            String StatusCode = array.getString("StatusCode");
            String Examsharing=array.getString("Examsharing");
            if(Examsharing.equalsIgnoreCase("NO"))
            {
              msg.onMessageReceived(message);
            }
            else
            {
              try {
                /*String[] examid =  message.split("\\@");*/
                String Exaid = array.getString("Filename");;
                String filelen=array.getString("Filelen");;
                      message_str=message;
                // unzip(file.getPath(),folder);

                Runnable runclint = new Examfile(Exaid, Long.parseLong(filelen));
                Thread recf = new Thread(runclint);
                recf.start();

              }
              catch (Exception e){

                e.printStackTrace();
              }
            }
          }
          catch (Exception e)
          {

          }
         //




        }

        else if(message.startsWith("SelfAssesmentExamquestion"))
        {

          msg.onMessageReceived("Filesharing");
          try {
            Thread.sleep(2000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          try {
            String exam_split[] = message.split("\\@");
            JSONObject array = new JSONObject(exam_split[1]);
            String status = array.getString("status");
            String StatusCode = array.getString("StatusCode");
            String Examsharing=array.getString("Examsharing");
            if(Examsharing.equalsIgnoreCase("NO"))
            {
              msg.onMessageReceived(message);
            }
            else
            {
              try {
                /*String[] examid =  message.split("\\@");*/
                String Exaid = array.getString("Filename");;
                String filelen=array.getString("Filelen");;
                message_str=message;
                // unzip(file.getPath(),folder);

                Runnable runclint = new Examfile(Exaid, Long.parseLong(filelen));
                Thread recf = new Thread(runclint);
                recf.start();

              }
              catch (Exception e){

                e.printStackTrace();
              }
            }
          }
          catch (Exception e)
          {

          }
         // msg.onMessageReceived(message);
        }

        else if(message.contains("Examsharing"))


        {


          //msg.onMessageReceived(message);
        }


        else if(message.contains("Pulsequestion"))
        {
          msg.onMessageReceived(message);
        }
        else if(message.contains("whiteboard"))
        {
          String whitesplit[]=message.split("\\@");
          //whiteboard@whiteboard:unsync
          if(whitesplit.length>1)
          {
            if(whitesplit[1].contains("sync"))
            {
              msg.onMessageReceived(message);
            }
            else  if(whitesplit[1].contains("unsync"))
            {
              msg.onMessageReceived(message);
            }
          }

        }
        else if(message.contains("lock"))
        {
          msg.onMessageReceived(message);

        }
        else if(message.equalsIgnoreCase("DND@true"))
        {
          msg.onMessageReceived(message);

        }
        else if(message.equalsIgnoreCase("DND@false"))
        {
          msg.onMessageReceived(message);

        }
        else if(message.startsWith("Quiz"))
        {
            msg.onMessageReceived(message);

        }else if(message.startsWith("Calendar"))
        {
          msg.onMessageReceived(message);

        }
        else if(message.startsWith("Timetable"))
        {
          msg.onMessageReceived(message);

        }


        else
        {

        }

      }

    /*  if(count>0)
      {
        byte b[]=new byte[count];
        clientData.readFully(b, 0, b.length);
        String str = new String(b, "UTF-8");
        Log.e("byte", "bytes" + str);


        String split[]=str.split("\\@");
        if(str.contains("Filesharing")) {
          Runnable runclint = new Receivefile(split[1], Long.parseLong(split[2]), socket.getInetAddress().toString());
          Thread recf = new Thread(runclint);
          recf.start();
        }
        else
        {
          Runnable runclint = new Examfile(split[1], Long.parseLong(split[2]), socket.getInetAddress().toString());
          Thread recf = new Thread(runclint);
          recf.start();
        }





      }*/


        JSONObject jsonObject = null;
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }





      return jsonObject;
    }


  }

  private String getMessageFromTheConnection(Socket connection) {
    String message = null;
    try {
      InputStreamReader isr = new InputStreamReader(new BufferedInputStream(connection.getInputStream()));
      StringBuilder process = new StringBuilder();
      synchronized (this) {
        while (true) {
          try {
            Log.e("reading","reading");
            int character = isr.read();
            Log.e("character",""+character);
            if (!(character == 13 || character == -1)) {
              process.append((char) character);
            }
            else
            {
              message = process.toString();
              return  message;
              //break;
            }
            message=process.toString();
            Log.e("reading","reading"+message);
          } catch (Exception e) {
            Log.e("catch","reading"+process.toString());

            e.printStackTrace();
          }
        }
        //Log.e("ending","end");

                /*  Log.e("messaging","msg"+message);
                  messagelistener.onMessageReceived(message);*/
      }


    } catch (IOException e1) {
      e1.printStackTrace();
    }
    return message;
  }

  private  class Receivefile implements Runnable {
    //private Socket socket;

    String fname;
    long flen;
    String ipaddress;
    String contentid;
    String subjectid;
    String type;
    String fileoriginal;

    Receivefile(String filename,long filelenth,String type,String subjectid,String contentid,String ip,String fileoriginalname) {

      fname=filename;
      flen=filelenth;
      ipaddress=ip;
      this.contentid=contentid;
      this.subjectid=subjectid;
      this.type=type;
      this.fileoriginal=fileoriginalname;

    }

    @Override
    public void run() {


      Socket getfilesocket= null;
      try {



        getfilesocket = new Socket(pref.getString("serverip",""),2222);
      } catch (IOException e) {

        msg.onMessageReceived("FileFailed");

        e.printStackTrace();
      }
      InputStream in = null;
      try {
        String filepath;
        in = getfilesocket.getInputStream();
        if(type.equalsIgnoreCase("academic")){
          filepath=pref.getString("academic","")+"/";
        }else {
          filepath=pref.getString("reference","")+"/";
        }
        Cursor c=obj.retriveContentvalues(Integer.parseInt(contentid));
        if(c.getCount()==0){
          ContentValues cv=new ContentValues();
          cv.put("ContentID",Integer.parseInt(contentid));
          Log.d("contentid",contentid);
          cv.put("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",fname);
          obj.insertNewContent(cv);
        }
        else if(c.getCount()!=0){
          ContentValues cv=new ContentValues();
          cv.put("ContentDescription",fileoriginal);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);

          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",filepath+fname);
          obj.updateContent(cv,Integer.parseInt(contentid));
        }


        int bytesRead;
        int current = 0;
      DataInputStream clientData = new DataInputStream(in);
      File file = new File(
              filepath,
              fname);
      OutputStream output = new FileOutputStream(file);
      long size = flen;
      byte[] buffer = new byte[1024];
      while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
      {
        output.write(buffer, 0, bytesRead);
        size -= bytesRead;
      }


      // Closing the FileOutputStream handle
      in.close();
      clientData.close();
      output.close();
        getfilesocket.close();
        if(file.length()==flen) {
          msg.onMessageReceived("File Received");
        }
        else
        {
          try {
            obj.deleteretriveContentvalues(Integer.parseInt(contentid));
          }
          catch (Exception e)
          {

          }
          msg.onMessageReceived("FileFailed");

        }


      } catch (IOException e) {

        try {
          obj.deleteretriveContentvalues(Integer.parseInt(contentid));
        }
        catch (Exception e1)
        {

        }
        msg.onMessageReceived("FileFailed");

        e.printStackTrace();
      }
      //File Received

    }
  }

  private  class Timetable implements Runnable {
    //private Socket socket;

    String fname;
    long flen;
    String ipaddress;
    String contentid;
    String subjectid;
    String type;
    String fileoriginal;
    int cid;
    int bid;

    Timetable(int classid,int batchid,String filename,long filelenth) {

      fname=filename;
      flen=filelenth;
      cid=classid;
      bid=batchid;

      //this.fileoriginal=fileoriginalname;

    }

    @Override
    public void run() {


      Socket getfilesocket= null;
      try {



        getfilesocket = new Socket(pref.getString("serverip",""),2222);
      } catch (IOException e) {
        msg.onMessageReceived("FileFailed");

        e.printStackTrace();
      }
      InputStream in = null;
      try {
        String filepath;
        in = getfilesocket.getInputStream();

          filepath=pref.getString("timetable","")+"/";


        int batid=0;
        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
          batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }
        if(batid==bid){
          ContentValues batcv = new ContentValues();
          batcv.put("BatchID",batid);

          batcv.put("TimeTable",filepath+fname);
          obj.UpdateTimeTable(batcv,batid);
          //con.insert("tblBatch",null,batcv);
        }else{

        }



       /* Cursor c=obj.retriveContentvalues(Integer.parseInt(contentid));
        if(c.getCount()==0){
          ContentValues cv=new ContentValues();
          cv.put("ContentID",Integer.parseInt(contentid));
          Log.d("contentid",contentid);
          cv.put("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",fname);
          obj.insertNewContent(cv);
        }
        else if(c.getCount()!=0){
          ContentValues cv=new ContentValues();
          cv.put("ContentDescription",fileoriginal);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);

          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",filepath+fname);
          obj.updateContent(cv,Integer.parseInt(contentid));
        }*/


        int bytesRead;
        int current = 0;
        DataInputStream clientData = new DataInputStream(in);
        File file = new File(
                filepath,
                fname);
        OutputStream output = new FileOutputStream(file);
        long size = flen;
        byte[] buffer = new byte[1024];
        while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
        {
          output.write(buffer, 0, bytesRead);
          size -= bytesRead;
        }


        // Closing the FileOutputStream handle
        in.close();
        clientData.close();
        output.close();
        getfilesocket.close();

        if(file.length()==flen) {
          msg.onMessageReceived("File Received");
        }
        else
        {

          msg.onMessageReceived("FileFailed");

        }



      } catch (IOException e) {
        msg.onMessageReceived("FileFailed");

        e.printStackTrace();
      }
     // msg.onMessageReceived("File Received");

    }
  }
//whiteboard


  private  class whiteboard implements Runnable {
    //private Socket socket;

    String fname;
    long flen;
    String ipaddress;
    String contentid;
    String subjectid;
    String type;
    String fileoriginal;
    int cid;
    int bid;

    whiteboard(String filename,long filelenth) {

      fname=filename;
      flen=filelenth;


      //this.fileoriginal=fileoriginalname;

    }

    @Override
    public void run() {


      Socket getfilesocket= null;
      try {



        getfilesocket = new Socket(pref.getString("serverip",""),2222);
      } catch (IOException e) {
        e.printStackTrace();
      }
      InputStream in = null;
      try {
        String filepath;
        in = getfilesocket.getInputStream();

        filepath=pref.getString("notes","")+"/";


      /*  int batid=0;
        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
          batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }
        if(batid==bid){
          ContentValues batcv = new ContentValues();
          batcv.put("BatchID",batid);

          batcv.put("TimeTable",filepath+fname);
          obj.UpdateTimeTable(batcv,batid);
          //con.insert("tblBatch",null,batcv);
        }else{

        }
*/


       /* Cursor c=obj.retriveContentvalues(Integer.parseInt(contentid));
        if(c.getCount()==0){
          ContentValues cv=new ContentValues();
          cv.put("ContentID",Integer.parseInt(contentid));
          Log.d("contentid",contentid);
          cv.put("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",fname);
          obj.insertNewContent(cv);
        }
        else if(c.getCount()!=0){
          ContentValues cv=new ContentValues();
          cv.put("ContentDescription",fileoriginal);
          Log.d("ContentDescription",fileoriginal);
          cv.put("Subject",subjectid);

          cv.put("Catalog",type);
          Log.d("Catalog",type);
          cv.put("ContentFilename",filepath+fname);
          Log.d("ContentFilename",filepath+fname);
          obj.updateContent(cv,Integer.parseInt(contentid));
        }*/


        int bytesRead;
        int current = 0;
        DataInputStream clientData = new DataInputStream(in);
        File file = new File(
                filepath,
                fname);
        OutputStream output = new FileOutputStream(file);
        long size = flen;
        byte[] buffer = new byte[1024];
        while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
        {
          output.write(buffer, 0, bytesRead);
          size -= bytesRead;
        }


        // Closing the FileOutputStream handle
        in.close();
        clientData.close();
        output.close();
        getfilesocket.close();


      } catch (IOException e) {
        e.printStackTrace();
      }
      msg.onMessageReceived("File Received");

    }
  }


  //unzip


  private  class Examfile implements Runnable {
    //private Socket socket;

    String fname;
    long flen;
    String ipaddress;

    Examfile(String filename,long filelenth) {

      fname=filename;
      flen=filelenth;
      //ipaddress=ip;
    }

    @Override
    public void run() {


      Socket getfilesocket= null;
      try {
        getfilesocket = new Socket(pref.getString("serverip",""),2223);
      } catch (IOException e) {
        msg.onMessageReceived("FileFailed");

        e.printStackTrace();
      }
      InputStream in = null;
      try {
        in = getfilesocket.getInputStream();

        int bytesRead;
        int current = 0;
        DataInputStream clientData = new DataInputStream(in);

        String Evaluation = pref.getString("Evaluation", "0");
        File username=new File(Evaluation+"/"+fname);
       /* File file = new File(
                Environment.getExternalStorageDirectory(),
                fname);*/
        OutputStream output = new FileOutputStream(username);
        long size = flen;
        byte[] buffer = new byte[1024];
        while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
        {
          output.write(buffer, 0, bytesRead);
          size -= bytesRead;
        }


        // Closing the FileOutputStream handle
        in.close();
        clientData.close();
        output.close();
        getfilesocket.close();
        String path=username.getAbsolutePath();
        String split[] =fname.split("\\.");
        String folder=path.substring(0,path.lastIndexOf("/"))+"/"+split[0];
       // unzip(folder,username.getPath());
      unpackZip(username.getPath(),folder+"/");
      } catch (IOException e) {
       // in.close();
        /*try {
          getfilesocket.close();
        } catch (IOException e1) {
          e1.printStackTrace();
        }*/

        e.printStackTrace();
      }


    }
  }

  private boolean unpackZip(String path, String  folder)
  {
    File destDir = new File(folder);
    if (!destDir.exists()) {
      destDir.mkdir();
    }
    InputStream is;
    ZipInputStream zis;
    try
    {
      String filename;
      is = new FileInputStream(path);
      zis = new ZipInputStream(new BufferedInputStream(is));
      ZipEntry ze;
      byte[] buffer = new byte[1024];
      int count;

      while ((ze = zis.getNextEntry()) != null)
      {
        // zapis do souboru
        filename = ze.getName();

        // Need to create directories if not exists, or
        // it will generate an Exception...
        if (ze.isDirectory()) {
          File fmd = new File(folder + filename);
          fmd.mkdirs();
          continue;
        }

        FileOutputStream fout = new FileOutputStream(folder + filename);

        // cteni zipu a zapis
        while ((count = zis.read(buffer)) != -1)
        {
          fout.write(buffer, 0, count);
        }

        fout.close();
        zis.closeEntry();
      }

      zis.close();
      msg.onMessageReceived(message_str);

    }
    catch(IOException e)
    {
      e.printStackTrace();
      msg.onMessageReceived("FileFailed");

      return false;
    }

    return true;
  }
  public void unzip(String zipFilePath, String destDirectory) throws IOException {
    File destDir = new File(destDirectory);
    if (!destDir.exists()) {
      destDir.mkdir();
    }

    ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
    ZipEntry entry = zipIn.getNextEntry();
    // iterates over entries in the zip file
    while (entry != null) {
      String filename;
      if(entry.getName().contains("/"))
      {
        filename=entry.getName();
        filename=filename.substring(filename.lastIndexOf("/")+1,filename.length());
      }
      else
      {
        filename=entry.getName();
      }
      String filePath = destDirectory + File.separator + filename;
      if (!entry.isDirectory()) {
        // if the entry is a file, extracts it
        extractFile(zipIn, filePath);
      } else {
        // if the entry is a directory, make the directory
        File dir = new File(filePath);
        dir.mkdir();
      }
      zipIn.closeEntry();
      entry = zipIn.getNextEntry();
    }
    zipIn.close();
    msg.onMessageReceived(message_str);
  }
  /**
   * Extracts a zip entry (file entry)
   * @param zipIn
   * @param filePath
   * @throws IOException
   */
  private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
    byte[] bytesIn = new byte[BUFFER_SIZE];
    int read = 0;
    while ((read = zipIn.read(bytesIn)) != -1) {
      bos.write(bytesIn, 0, read);
    }
    bos.close();
  }


  private boolean unpackZip(File file)
  {
    String path=file.getAbsolutePath();
    String folder=path.substring(0,path.lastIndexOf("/"))+"/";


    InputStream is;
    ZipInputStream zis;
    try
    {
      String filename;
      is = new FileInputStream(path);
      zis = new ZipInputStream(new BufferedInputStream(is));
      ZipEntry ze;
      byte[] buffer = new byte[1024];
      int count;

      while ((ze = zis.getNextEntry()) != null)
      {
        // zapis do souboru
        filename = ze.getName();

        // Need to create directories if not exists, or
        // it will generate an Exception...
        if (ze.isDirectory()) {
          File fmd = new File(folder+filename);
          fmd.mkdirs();
          continue;
        }
        else
        {

        }

        FileOutputStream fout = new FileOutputStream(folder+filename);

        // cteni zipu a zapis
        while ((count = zis.read(buffer)) != -1)
        {
          fout.write(buffer, 0, count);
        }

        fout.close();
        zis.closeEntry();
      }

      zis.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      return false;
    }

    return true;
  }

 public  void closesocket()
  {
    try
    {
      if(serverSocket!=null)
      {
        serverSocket.close();;
      }
    }
    catch(Exception e)
    {

    }
  }


}
