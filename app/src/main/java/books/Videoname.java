package books;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Videoname {


    String name = null;




    String bookpath;

    String contenttitle=null;
    boolean selected = false;

    public Videoname(){

    }
    public Videoname(String videopath, String name, boolean selected) {
        super();

        this.name = name;
        this.selected = selected;
    }
    public String getBookpath() {
        return bookpath;
    }

    public void setBookpath(String bookpath) {
        this.bookpath = bookpath;
    }

    public String getContenttitle() {
        return contenttitle;
    }

    public void setContenttitle(String contenttitle) {
        this.contenttitle = contenttitle;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
