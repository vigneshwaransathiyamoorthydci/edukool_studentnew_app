package books;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Method;

import com.dci.edukool.student.R;

/**
 * Created by kirubakaranj on 5/30/2017.
 */

public class ImageActivity extends AppCompatActivity {

    String imagepath;
    ImageView image;
    ImageView close;
    BroadcastReceiver movepage;

    public static Activity imageact=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.image_activity);
        image=(ImageView)findViewById(R.id.image);
        close=(ImageView)findViewById(R.id.close);

        Intent i=getIntent();
        Bundle bun=i.getExtras();
        imagepath= bun.getString("path");
        imageact=ImageActivity.this;


        if (imagepath != null) setbackground(image, imagepath);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageActivity.this.finish();
            }
        });

    }

    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
        /*float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
        movepage=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

				/*Intent in=new Intent("filemove");
				in.putExtra("page",file[1].toString());
				sendBroadcast(in);*/
                try

                {
                    if(intent.getExtras().getBoolean("close",false))
                    {
                       // Toast.makeText(ImageActivity.this,"close",Toast.LENGTH_LONG).show();

                        finish();
                    }
                    else {
						/*Toast.makeText(MuPDFActivity.this,"closenot",Toast.LENGTH_LONG).show();
*/
                        // mDocView.setDisplayedViewIndex(Integer.parseInt(intent.getStringExtra("page")));

                    }

                }
                catch (Exception e)
                {

                }

            }
        };
        IntentFilter in=new IntentFilter("filemove");
        registerReceiver(movepage,in);
    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(movepage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

}
