package books;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import Utils.*;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Utils.Utils;
import helper.RoundedImageView;
import steadytate.customviewgroup;

import com.dci.edukool.student.Bookpojo;
import com.dci.edukool.student.ContentPojo;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

public class BookBinActivity extends Activity {

 

    private int mQuestionIndex = 0;
    
	LinearLayout hlvCustomList;	
   
 //   SelfTestQuestionAdapter selfevalutation_adapter;

    private Button layoutinputButton;
   // ListView shelflistView;
    GridView bookbinlist;
    List<String> li;
    Multimediaadapter dataAdapter = null;
    ArrayList<Videoname>videos;
    ArrayList<Bookpojo> booklist;
    ImageView exit;
    ImageView back,down;
    RoundedImageView profileimage;
    TextView textView;
    ArrayList<String>bookshelf;
    ArrayList<String>bookshelfid;
    View header;
    String stdname,profilename;
    Videoname vname;
    GridView listView;

    String url="http://api.schoolproject.dci.in/api/";
    int stdid,batid;
    String stdlogin_id;
    Utils utils;
    SharedPreferences pref;
   String academicpath,refpath;
    SqliteOpenHelperDemo obj;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookbinmain);
        /*try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/

        if(Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(BookBinActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
           // disablenotification();
        }
        else {
           // disablenotification();
        }
    
		hlvCustomList = (LinearLayout)findViewById(R.id.hlvCustomList);
        exit= (ImageView) findViewById(R.id.exit);
        down= (ImageView) findViewById(R.id.handrise);
        profileimage= (RoundedImageView) findViewById(R.id.profileimage);
        back= (ImageView) findViewById(R.id.back);
        textView= (TextView) findViewById(R.id.textView);
      //  textView.setText(MainActivity.studentName);
        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }

        utils=new Utils(BookBinActivity.this);

        obj=new SqliteOpenHelperDemo(getApplicationContext());

        bookshelf=new ArrayList<>();
        bookshelfid=new ArrayList<>();


        Cursor stdcur=obj.retrive("tblStudent");
        while(stdcur.moveToNext()){
         profilename= stdcur.getString(stdcur.getColumnIndex("PhotoFilename"));
         stdid =stdcur.getInt(stdcur.getColumnIndex("StudentID"));
         stdlogin_id =stdcur.getString(stdcur.getColumnIndex("StudentLoginID"));
        }
        if(profilename!=null)setbackground(profileimage,profilename);


        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
            batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }

        Cursor subcursor=obj.retriveSubjects(batid);
        while(subcursor.moveToNext()){
            bookshelfid.add(subcursor.getString(subcursor.getColumnIndex("SubjectID")));
            bookshelf.add(subcursor.getString(subcursor.getColumnIndex("SubjectName")));
        }
        for(int i=0;i<bookshelf.size();i++){
     // Toast.makeText(getApplicationContext(),bookshelf.get(i)+","+bookshelfid.get(i),Toast.LENGTH_LONG).show();
        }



        videos=new ArrayList<>();

        booklist=new ArrayList<Bookpojo>();




        Cursor c1=obj.retrive("tblContent");


        while(c1.moveToNext()) {
            Bookpojo bookpojo=new Bookpojo();
            bookpojo.setBookpath(c1.getString(c1.getColumnIndex("ContentFilename")));
            bookpojo.setSubject(c1.getString(c1.getColumnIndex("Subject")));
            bookpojo.setType(c1.getString(c1.getColumnIndex("ContentCatalogType")));
            bookpojo.setCatalog(c1.getString(c1.getColumnIndex("Catalog")));
            bookpojo.setBookname(c1.getString(c1.getColumnIndex("ContentDescription")));
            booklist.add(bookpojo);
        }


        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            stdname= tc.getString(tc.getColumnIndex("FirstName"));
        }

        textView.setText(stdname.toUpperCase());

        try {
			bookbinload();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       // Search_Dir(Environment.getExternalStorageDirectory());

        loadbookshelf(0);


        dataAdapter = new Multimediaadapter(this,
                R.layout.booklistitem,videos );
        listView = (GridView) findViewById(R.id.booklist);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!pref.getBoolean("classtime",false)) {
                    new validateUserTask().execute("");
                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.session),Toast.LENGTH_LONG).show();
                }
            }
        });

        pref=getSharedPreferences("student",MODE_PRIVATE);
        academicpath=pref.getString("academic","");
        refpath=pref.getString("reference","");
    }




    public void Loadlist(){

        booklist.clear();
        Cursor c1=obj.retrive("tblContent");

        while(c1.moveToNext()) {
            Bookpojo bookpojo=new Bookpojo();
            bookpojo.setBookpath(c1.getString(c1.getColumnIndex("ContentFilename")));
            bookpojo.setSubject(c1.getString(c1.getColumnIndex("Subject")));
            bookpojo.setType(c1.getString(c1.getColumnIndex("ContentCatalogType")));
            bookpojo.setCatalog(c1.getString(c1.getColumnIndex("Catalog")));
            bookpojo.setBookname(c1.getString(c1.getColumnIndex("ContentDescription")));
            booklist.add(bookpojo);
        }

      loadbookshelf(0);
    }


    private class validateUserTask extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(BookBinActivity.this);
            dia.setMessage("DOWNLOADING...");
            dia.setCancelable(false);
            dia.show();}
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
           /* postParameters.add(new BasicNameValuePair("Function", "SyncBookbin"));
            postParameters.add(new BasicNameValuePair("UserId", ""+stdid));
            postParameters.add(new BasicNameValuePair("UserType", "Student"));
            postParameters.add(new BasicNameValuePair("UserName", stdlogin_id));*/
            String userid="Function:SyncBookbin|UserId:"+stdid+"|UserType:Student|UserName:"+stdlogin_id;

            String res = null;
            byte[] data ;

            try {
                data = userid.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                if(utils.hasConnection()) {
                    postParameters.clear();
                    postParameters.add(new BasicNameValuePair("WS", base64_register));
                    Service sr = new Service(BookBinActivity.this);
                    res = sr.getLogin(postParameters,
                            Url.base_url);
                   /* response = CustomHttpClient.executeHttpPost(params[0], postParameters);*/
                   // res = response.toString();
                    Log.e("vikis", "resoibse"+ res + ",,");
                }
                else
                {
                    utils.Showalert();
                }

            }
            catch (Exception e) {
               // txt_Error.setText(e.toString());
                Log.e("Exception",e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Log.e("vikis", "result"+ result + ",,");
            if(dia.isShowing())
                dia.cancel();
             new Other(result).execute("");

            //Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_LONG).show();

        }//close onPostExecute
    }// close validateUserTask




    private class Other extends AsyncTask<String, Void, String> {
        String result, filepath;

        Other(String result) {
            this.result = result;

        }

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(BookBinActivity.this);
            dia.setMessage("DOWNLOADING...");
            dia.setCancelable(false);
            dia.show();}

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;



            try {
                JSONObject jObj = new JSONObject(result);
                String statusCode = jObj.getString("StatusCode");

                if (statusCode.toString().equalsIgnoreCase("200")) {

                    ArrayList<ContentPojo> Contentlist = new ArrayList<ContentPojo>();
                    for (int i = 0; i < jObj.getJSONArray("contentDetails").length(); i++) {
                        ContentPojo contentpojo = new ContentPojo();
                        Cursor c = obj.retriveIsContentId(Integer.parseInt(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ID")));
                        if (c.getCount() > 0==false)
                        {
                            contentpojo.setContentid(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ID"));
                            contentpojo.setContentDescription(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentTitle"));
                            contentpojo.setContentType(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentType"));
                            contentpojo.setCatalog(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog"));
                            contentpojo.setVaporize(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Vaporize"));
                            contentpojo.setSubject(jObj.getJSONArray("contentDetails").getJSONObject(i).getJSONArray("Subject").toString());


                            String urls = jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentFileName");


                            if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Academic")) {

                                otherTask(urls, academicpath);

                                String content_filename = academicpath + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename);

                            } else if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Reference")) {

                                otherTask(urls, refpath);


                                String content_filename1 = refpath + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename1);
                            }



                            Contentlist.add(contentpojo);

                        }
                        if(c.getCount()>0){

                            contentpojo.setContentid(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ID"));
                            contentpojo.setContentDescription(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentTitle"));
                            contentpojo.setContentType(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentType"));
                            contentpojo.setCatalog(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog"));
                            contentpojo.setVaporize(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Vaporize"));
                            contentpojo.setSubject(jObj.getJSONArray("contentDetails").getJSONObject(i).getJSONArray("Subject").toString());


                            String urls = jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentFileName");


                            if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Academic")) {

                                otherTask(urls, academicpath);

                                String content_filename = academicpath + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename);

                            } else if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Reference")) {

                                otherTask(urls, refpath);


                                String content_filename1 = refpath + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename1);
                                obj.UpdateContentDownload(contentpojo);
                            }

                        }

                    }
                    if(Contentlist.size()!=0)
                        obj.InsertContent(Contentlist);


                }
            }catch(Exception e){}




            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String resultdup) {
            if(dia.isShowing())
                dia.cancel();
            Loadlist();
            new validateUserTask2(result).execute(url);

        }
    }

    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response,statusres;

        validateUserTask2(String statusres){
            this.statusres=statusres;
        }

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(BookBinActivity.this);
            dia.setMessage("Downloading...");
            dia.setCancelable(false);
            dia.show();}
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:StudentSyncComplete|StudentID:" + stdid + "|UserName:" + stdlogin_id + "|Type:content";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(BookBinActivity.this);

                        res = sr.getLogin(postParameters,
                                Url.base_url);
                       // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                      //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                }
                else{
                Log.e("error","ss");
                }

                }
            catch(Exception e){
                    // txt_Error.setText(e.toString());
                    Log.e("Exception", e.toString());
                }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
              if(dia.isShowing())
                dia.cancel();
            try {
                JSONObject jObj = new JSONObject(result);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {
                    Toast.makeText(getApplicationContext(),"Academic data downloaded successfully.",Toast.LENGTH_LONG).show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Downloading data failed.",Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {

            }
           // Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
        }//close onPostExecute
    }// close validateUserTask



    public void otherTask(String urls,String filepath){


        int count;
        try {
            String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());

            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();


            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);
            // Output stream
            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
        }



    }




public void setValue(String positionsub){
    videos.clear();
    Boolean click=false;
    int clicktoast=0;

    for(int i=0;i<booklist.size();i++){
        String subarray=booklist.get(i).getSubject();
       String pdfcheck=booklist.get(i).getBookpath();
        //  subarray=String str = "[Chrissman-@1]";

      try {
       //   if (pdfcheck.contains(".pdf")) {
          if(booklist.get(i).getCatalog().equalsIgnoreCase("Academic")) {
              subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
              subarray = subarray.replace("\"", "");
              //  Log.d("subarra", subarray);
              vname = new Videoname();
              // subarray="1,7";

              if (subarray.contains(",")) {
                  String[] separated = subarray.split(",");
                  for (int sep = 0; sep < separated.length; sep++) {
                      //     Log.d("separa",separated[sep]);
                      //   Log.d("posit",positionsub);
                      if (separated[sep].equals(positionsub)) {
                          // if (separated[sep].equals(bookshelfid.get(0))) {

                          vname.setContenttitle(booklist.get(i).getBookname());
                          vname.setBookpath(booklist.get(i).getBookpath());
                          videos.add(vname);
                          dataAdapter = new Multimediaadapter(this,
                                  R.layout.booklistitem, videos);
                          listView = (GridView) findViewById(R.id.booklist);
                          // Assign adapter to ListView
                          listView.setAdapter(dataAdapter);

                      } else {

                      }
                  }
              } else if (subarray.equals(positionsub)) {

                  vname.setContenttitle(booklist.get(i).getBookname());
                  vname.setBookpath(booklist.get(i).getBookpath());
                  videos.add(vname);
                  dataAdapter = new Multimediaadapter(this,
                          R.layout.booklistitem, videos);
                  listView = (GridView) findViewById(R.id.booklist);
                  // Assign adapter to ListView
                  listView.setAdapter(dataAdapter);
              } else {

              }

          }
        //  }//pdfcheck
      }catch(NullPointerException e) {
          Log.d("Exception", "" + e.toString());
      }
    }
    if(videos.size()>0)
    {

    }
    else
    {
        Toast.makeText(getApplicationContext(), "No Academic Books Available", Toast.LENGTH_SHORT).show();

    }



}
    private void bookbinload() throws Exception {}
    
    public void Search_Dir(File dir) {
     
        String videopatten2 = ".pdf";

        //String videopatten1 = ".wav";
      /*  String videopatten = ".pdf";
        String videopatten = ".pdf";
*/
        Log.d("check",
                "Environment.getExternalStorageDirectory()------"
                        + dir.getName());

        String filename =  File.separator + "CSR_101" + File.separator + "User"+  File.separator + "Kumaran"+  File.separator + "Material";
        System.out.println(filename + "filename");
        File f = new File(Environment.getExternalStorageDirectory(), filename);
        System.out.println(f + "f");

        File FileList[] = f.listFiles();
     /*   Log.d("check",
                "filelist length---- "
                        + FileList.length);
*/
        if (FileList != null) {
            for (int i = 0; i < FileList.length; i++) {

                if (FileList[i].isDirectory()) {
                    Search_Dir(FileList[i]);
                } else {

                    Log.d("check",
                            "for check from .pdf---- "
                                    + FileList[i].getName());
                    if (FileList[i].getName().endsWith(videopatten2)) {
                        // here you have that file.
                       // pdfArrayList.add(FileList[i].getPath());
                        Videoname video=new Videoname(FileList[i].getPath(),FileList[i].getName(),false);

                        videos.add(video);
                    }
                }
            }
        }

    }
    
    void loadbookshelf(int show)
    {
    	hlvCustomList.removeAllViews();
    	for(int i=0; i<bookshelf.size(); i++)
    	{
    		final View hiddenInfo = getLayoutInflater().inflate(
					R.layout.bookself, hlvCustomList, false);
    		final TextView bookshelftext=(TextView) hiddenInfo.findViewById(R.id.shelftext);
            final TextView colortext=(TextView) hiddenInfo.findViewById(R.id.shelf);
    		bookshelftext.setText(bookshelf.get(i));
    	    bookshelftext.setTag(bookshelfid.get(i));
            colortext.setTag(i);
    	    hlvCustomList.addView(hiddenInfo);
           if(i==0){
                 setValue(String.valueOf(bookshelftext.getTag()));
           }
            if(show==i)
            {

           hiddenInfo.setBackgroundResource(R.color.appbg);
            }


            hiddenInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {





                    loadbookshelf((int)colortext.getTag()); //// this for color change in item
                    setValue(String.valueOf(bookshelftext.getTag()));
//Toast.makeText(getApplicationContext(),"posit"+bookshelftext.getTag(),Toast.LENGTH_LONG).show();
                }

            });
    	}




    }
    void setbackground(ImageView view, String filepath)
    {
        try {


        File imgFile=new File(filepath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
        }
        catch (Exception e)
        {

        }
    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue=0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
        /*float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }


    void disablenotification()
    {
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (25 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        customviewgroup view = new customviewgroup(this);

        manager.addView(view, localLayoutParams);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}




